using System;
using System.Data.Entity.Migrations;

namespace Osnova.Migrations
{
	
	public partial class EcoJira : DbMigration
	{
		public override void Up()
		{
			CreateView_Issue();
			CreateView_IssueLink();
			CreateView_Project();
			CreateView_Worklog();
		}//function
        
		public override void Down()
		{
			Sql("DROP VIEW [dbo].[EntityEcoIssue] ");
			Sql("DROP VIEW [dbo].[EntityEcoIssueLink] ");
			Sql("DROP VIEW [dbo].[EntityEcoProject] ");
			Sql("DROP VIEW [dbo].[EntityEcoWorklog] ");
		}//function

		void CreateView_Issue()
		{
			string sql = @"CREATE VIEW [dbo].[EntityEcoIssue]
AS
SELECT     i.ID, i.PROJECT AS ProjectID, i.issuenum AS Code, i.SUMMARY AS Name, i.DESCRIPTION AS Note, i.REPORTER, i.ASSIGNEE, t.pname AS IssueType, 
                      s.pname AS IssueStatus, i.RESOLUTIONDATE, i.CREATOR, i.CREATED AS CreateDate, i.UPDATED AS ChangeDate, 0 AS UserCreate, 0 AS UserChange
, cast (1 as timestamp) as RowVersion, NULL as Stat, NULL as UserResponsible, NULL as UserFix
FROM         dbo.jirajiraissue AS i INNER JOIN
                      dbo.jiraissuetype AS t ON t.ID = i.issuetype INNER JOIN
                      dbo.jiraissuestatus AS s ON s.ID = i.issuestatus";

			Sql(sql);
		}//function

		void CreateView_IssueLink()
		{
			string sql = @"CREATE VIEW [dbo].[EntityEcoIssueLink]
AS
select l.ID, DESTINATION as ParentID, SOURCE as IssueID, t.LinkName 
, GETDATE() as CreateDate, GETDATE() as ChangeDate
, 0 as UserCreate, 0 as UserChange
, cast (1 as timestamp) as RowVersion, NULL as Stat, NULL as UserResponsible, NULL as UserFix
FROM  [dbo].[jiraissuelink] l
join [dbo].[jiraissuelinktype] t on t.ID = l.LINKTYPE
";

			Sql(sql);
		}//function

		void CreateView_Project()
		{
			string sql = @"CREATE VIEW [dbo].[EntityEcoProject]
AS
select ID, pkey as CODE, pname as Name, DESCRIPTION, LEAD
, GETDATE() as CreateDate, GETDATE() as ChangeDate
, 0 as UserCreate, 0 as UserChange
, cast (1 as timestamp) as RowVersion, NULL as Stat, NULL as UserResponsible, NULL as UserFix
from [dbo].[jiraproject]
";

			Sql(sql);
		}//function

		void CreateView_Worklog()
		{
			string sql = @"CREATE VIEW [dbo].[EntityEcoWorklog]
AS
select ID, IssueID, Author, StartDate as DtWork
, (timeworked/3600) as Hours, timeworked as Seconds
, CREATED as CreateDate, UPDATED as ChangeDate
, 0 as UserCreate, 0 as UserChange
, cast (1 as timestamp) as RowVersion, NULL as Stat, NULL as UserResponsible, NULL as UserFix
  FROM [dbo].[jiraworklog] 
";

			Sql(sql);
		}//function
	}//class
}//ns
