using System;
using System.Data.Entity.Migrations;

namespace Osnova.Migrations
{
	public partial class JIRA : DbMigration
	{
		public override void Up()
		{
			CreateTable_JiraIssue();
			CreateTable_JiraIssueLink();
			CreateTable_JiraIssueLinkType();
			CreateTable_JiraIssueStatus();
			CreateTable_JiraIssueType();
			CreateTable_JiraProject();
			CreateTable_JiraWorklog();
		}//function
        
		public override void Down()
		{
			DropTable("[dbo].[jirajiraissue]");
			DropTable("[dbo].[jiraissuelink]");
			DropTable("[dbo].[jiraissuelinktype]");
			DropTable("[dbo].[jiraissuestatus]");
			DropTable("[dbo].[jiraissuetype]");
			DropTable("[dbo].[jiraproject]");
			DropTable("[dbo].[jiraworklog]");
		}//function

		void CreateTable_JiraIssue()
		{
			string sql = @"CREATE TABLE [dbo].[jirajiraissue](
	[ID] [numeric](18, 0) NOT NULL,
	[pkey] [nvarchar](255) NULL,
	[PROJECT] [numeric](18, 0) NULL,
	[REPORTER] [nvarchar](255) NULL,
	[ASSIGNEE] [nvarchar](255) NULL,
	[issuetype] [nvarchar](255) NULL,
	[SUMMARY] [nvarchar](255) NULL,
	[DESCRIPTION] [ntext] NULL,
	[ENVIRONMENT] [ntext] NULL,
	[PRIORITY] [nvarchar](255) NULL,
	[RESOLUTION] [nvarchar](255) NULL,
	[issuestatus] [nvarchar](255) NULL,
	[CREATED] [datetime] NULL,
	[UPDATED] [datetime] NULL,
	[DUEDATE] [datetime] NULL,
	[RESOLUTIONDATE] [datetime] NULL,
	[VOTES] [numeric](18, 0) NULL,
	[TIMEORIGINALESTIMATE] [numeric](18, 0) NULL,
	[TIMEESTIMATE] [numeric](18, 0) NULL,
	[TIMESPENT] [numeric](18, 0) NULL,
	[WORKFLOW_ID] [numeric](18, 0) NULL,
	[SECURITY] [numeric](18, 0) NULL,
	[FIXFOR] [numeric](18, 0) NULL,
	[COMPONENT] [numeric](18, 0) NULL,
	[WATCHES] [numeric](18, 0) NULL,
	[issuenum] [numeric](18, 0) NULL,
	[CREATOR] [nvarchar](255) NULL,
 CONSTRAINT [PK_jiraissue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]";
			Sql(sql);
		}//function

		void CreateTable_JiraIssueLink()
		{
			string sql = @"CREATE TABLE [dbo].[jiraissuelink](
	[ID] [numeric](18, 0) NOT NULL,
	[LINKTYPE] [numeric](18, 0) NULL,
	[SOURCE] [numeric](18, 0) NULL,
	[DESTINATION] [numeric](18, 0) NULL,
	[SEQUENCE] [numeric](18, 0) NULL,
 CONSTRAINT [PK_issuelink] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]";
			Sql(sql);
		}//function

		void CreateTable_JiraIssueLinkType()
		{
			string sql = @"CREATE TABLE [dbo].[jiraissuelinktype](
	[ID] [numeric](18, 0) NOT NULL,
	[LINKNAME] [nvarchar](255) NULL,
	[INWARD] [nvarchar](255) NULL,
	[OUTWARD] [nvarchar](255) NULL,
	[pstyle] [nvarchar](60) NULL,
 CONSTRAINT [PK_issuelinktype] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]";
			Sql(sql);
		}//function

		void CreateTable_JiraIssueStatus()
		{
			string sql = @"CREATE TABLE [dbo].[jiraissuestatus](
	[ID] [nvarchar](60) NOT NULL,
	[SEQUENCE] [numeric](18, 0) NULL,
	[pname] [nvarchar](60) NULL,
	[DESCRIPTION] [ntext] NULL,
	[ICONURL] [nvarchar](255) NULL,
	[STATUSCATEGORY] [numeric](18, 0) NULL,
 CONSTRAINT [PK_issuestatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]";
			Sql(sql);
		}//function

		void CreateTable_JiraIssueType()
		{
			string sql = @"CREATE TABLE [dbo].[jiraissuetype](
	[ID] [nvarchar](60) NOT NULL,
	[SEQUENCE] [numeric](18, 0) NULL,
	[pname] [nvarchar](60) NULL,
	[pstyle] [nvarchar](60) NULL,
	[DESCRIPTION] [ntext] NULL,
	[ICONURL] [nvarchar](255) NULL,
	[AVATAR] [numeric](18, 0) NULL,
 CONSTRAINT [PK_issuetype] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]";
			Sql(sql);
		}//function

		void CreateTable_JiraProject()
		{
			string sql = @"CREATE TABLE [dbo].[jiraproject](
	[ID] [numeric](18, 0) NOT NULL,
	[pname] [nvarchar](255) NULL,
	[URL] [nvarchar](255) NULL,
	[LEAD] [nvarchar](255) NULL,
	[DESCRIPTION] [ntext] NULL,
	[pkey] [nvarchar](255) NULL,
	[pcounter] [numeric](18, 0) NULL,
	[ASSIGNEETYPE] [numeric](18, 0) NULL,
	[AVATAR] [numeric](18, 0) NULL,
	[ORIGINALKEY] [nvarchar](255) NULL,
 CONSTRAINT [PK_project] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]";
			Sql(sql);
		}//function

		void CreateTable_JiraWorklog()
		{
			string sql = @"CREATE TABLE [dbo].[jiraworklog](
	[ID] [numeric](18, 0) NOT NULL,
	[issueid] [numeric](18, 0) NULL,
	[AUTHOR] [nvarchar](255) NULL,
	[grouplevel] [nvarchar](255) NULL,
	[rolelevel] [numeric](18, 0) NULL,
	[worklogbody] [ntext] NULL,
	[CREATED] [datetime] NULL,
	[UPDATEAUTHOR] [nvarchar](255) NULL,
	[UPDATED] [datetime] NULL,
	[STARTDATE] [datetime] NULL,
	[timeworked] [numeric](18, 0) NULL,
 CONSTRAINT [PK_worklog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]";
			Sql(sql);
		}//function
	}//class
}//ns
