namespace Osnova.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Report : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EntityReport",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 250),
                        Name = c.String(nullable: false, maxLength: 250),
                        Description = c.String(nullable: false),
                        DypeCode = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.Code, unique: true, name: "IX_Report_Code")
                .Index(t => t.Name, unique: true, name: "IX_Report_Name");
            
            CreateTable(
                "dbo.EntityRoleReport",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RoleID = c.Int(nullable: false),
                        ReportID = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EntityReport", t => t.ReportID)
                .ForeignKey("dbo.EntityRole", t => t.RoleID)
                .Index(t => new { t.RoleID, t.ReportID }, unique: true, name: "IX_RoleReport");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EntityRoleReport", "RoleID", "dbo.EntityRole");
            DropForeignKey("dbo.EntityRoleReport", "ReportID", "dbo.EntityReport");
            DropIndex("dbo.EntityRoleReport", "IX_RoleReport");
            DropIndex("dbo.EntityReport", "IX_Report_Name");
            DropIndex("dbo.EntityReport", "IX_Report_Code");
            DropTable("dbo.EntityRoleReport");
            DropTable("dbo.EntityReport");
        }
    }
}
