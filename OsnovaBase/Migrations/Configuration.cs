namespace Osnova.Migrations
{
   using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
	using System.Collections.Generic;
	using Osnova.Model;
	using Osnova.Infrastructure;
	using Rpt = Osnova.Model.Report;
	using System.Data.Entity.Validation;

	internal sealed class Configuration : DbMigrationsConfiguration<Osnova.Infrastructure.OsnovaDB>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = true;
			AutomaticMigrationDataLossAllowed = true;
		}

		void FillDefaultForMigration(EntityBase e)
		{
				e.CreateDate = DateTime.Now;
				e.ChangeDate = DateTime.Now;
				e.UserCreate = 0;
				e.UserChange = 0;
		}//function

		protected override void Seed(Osnova.Infrastructure.OsnovaDB context)
		{
			#region clear previous data
			context.RunSQL("delete from dbo." + OsnovaDB.GetTableName<RoleDype>());
			context.RunSQL("delete from dbo." + OsnovaDB.GetTableName<RoleSpecial>());
			context.RunSQL("delete from dbo." + OsnovaDB.GetTableName<RoleStatus>());
			context.RunSQL("delete from dbo." + OsnovaDB.GetTableName<RoleStatusContent>());
			context.RunSQL("delete from dbo." + OsnovaDB.GetTableName<RoleReport>());
			context.RunSQL("delete from dbo." + OsnovaDB.GetTableName<RoleUser>());

			context.RunSQL("delete from dbo." + OsnovaDB.GetTableName<Special>());
			context.RunSQL("delete from dbo." + OsnovaDB.GetTableName<Status>());
			context.RunSQL("delete from dbo." + OsnovaDB.GetTableName<Dype>());
			context.RunSQL("delete from dbo." + OsnovaDB.GetTableName<Groupdype>());
			context.RunSQL("delete from dbo." + OsnovaDB.GetTableName<Role>());
			context.RunSQL("delete from dbo." + OsnovaDB.GetTableName<User>());
			context.RunSQL("delete from dbo." + OsnovaDB.GetTableName<Report>());

			Type[] ecoTypesDelete = { typeof(EcoEmplAbsent), typeof(EcoEmplAbsentType), typeof(EcoEmpl), typeof(EcoDepartment), typeof(EcoCompany) };
			foreach (var item in ecoTypesDelete)
			{
				context.RunSQL("delete from dbo." + OsnovaDB.GetTableName(item));	
			}//for

			context.RunSQL("delete from dbo." + OsnovaDB.GetTableName<SourceNews>());
			#endregion

			#region Seed
			Random rnd = new Random();
			string sqlReseed = "DBCC CHECKIDENT ('{0}', RESEED, {1})";
			Dictionary<Type, int> seed = new Dictionary<Type, int>();
			seed.Add(typeof(Dype), rnd.Next(100000));
			seed.Add(typeof(Groupdype), rnd.Next(100000));
			seed.Add(typeof(Role), rnd.Next(100000));
			seed.Add(typeof(User), rnd.Next(100000));
			seed.Add(typeof(Special), rnd.Next(100000));
			seed.Add(typeof(Status), rnd.Next(100000));
			foreach (var t in seed.Keys)
			{
				context.RunSQL(sqlReseed.Fmt(OsnovaDB.GetTableName(t), seed[t]));
			}//for

			var ecoTypesSeed = ecoTypesDelete;
			foreach (var item in ecoTypesDelete)
			{
				context.RunSQL(sqlReseed.Fmt(OsnovaDB.GetTableName(item), rnd.Next(100000)));
			}//for

			#endregion

			#region initialData
			Groupdype[] gdS =
			{
				new Groupdype { Code = "Admin", Name = "�����������������" },
				new Groupdype { Code = "Source", Name = "���������" },
			};
			gdS.ForEach(e => FillDefaultForMigration(e));
			context.GroupdypeSet.AddOrUpdate(gdS);
			Func<string, Groupdype> getGd = (code) => gdS.First(r => r.Code == code);

			Dype[] dypeS = 
			{
				new Dype { Code = "Dype", Name = "���", Groupdype = getGd("Admin") },
				new Dype { Code = "Groupdype", Name = "������ �����", Groupdype = getGd("Admin") },
				new Dype { Code = "Role", Name = "����", Groupdype = getGd("Admin") },
				new Dype { Code = "RoleDype", Name = "�������", Groupdype = getGd("Admin") },
				new Dype { Code = "RoleSpecial", Name = "������������", Groupdype = getGd("Admin") },
				new Dype { Code = "RoleStatusContent", Name = "��������������������", Groupdype = getGd("Admin") },
				new Dype { Code = "RoleUser", Name = "�������������", Groupdype = getGd("Admin") },
				new Dype { Code = "Special", Name = "��������", Groupdype = getGd("Admin") },
				new Dype { Code = "Status", Name = "������", Groupdype = getGd("Admin") },
				new Dype { Code = "User", Name = "���������", Groupdype = getGd("Admin") },
				new Dype { Code = "SourceNews", Name = "�������� ��������", Groupdype = getGd("Source") },
			};
			dypeS.ForEach(e => FillDefaultForMigration(e)); 
			context.DypeSet.AddOrUpdate(dypeS);
			Func<string, Dype> getDype = (code) => dypeS.First(r => r.Code == code);

			//special
			Special[] spS = 
			{
				new Special { Code = "Test", Name = "��������", Dype = getDype("SourceNews") }
			};
			spS.ForEach(e => FillDefaultForMigration(e));
			context.Set<Special>().AddOrUpdate(spS);

			//������
			string contTesting = "From=Done DoneBad".AddLine("Check=IsFalse");
			string contDone = "From=NULL".AddLine("Check=IsTrue IsComplete", "Run=AddZ", "Readonly");
			string contDoneBad = "From=NULL".AddLine("Run=AddZ IsFalse");
			Status[] stS = 
			{ 
				new Status { Code = "Testing", Name = "��������", Dype = getDype("User"), Content = contTesting },
				new Status { Code = "Done", Name = "������", Dype = getDype("User"), Content = contDone },
				new Status { Code = "DoneBad", Name = "������ BAD", Dype = getDype("User"), Content = contDoneBad },
				new Status { Code = "NoAccess", Name = "��� �������", Dype = getDype("User"), Content = contTesting },
			};
			stS.ForEach(e => FillDefaultForMigration(e));
			context.Set<Status>().AddOrUpdate(stS);
			Func<string, Status> getStatus = (code) => stS.First(r => r.Code == code);

			//�����
			Rpt[] rptS = 
			{ 
				new Rpt { Code = "EcoEmplAbsentToday", Name = "���������� ����������� �� �������", Description="nope", Parameters = string.Empty },
				new Rpt { Code = "ProjectStat", Name = "���������� �� �������", Description="���������� �� �������", DypeCode=typeof(EcoProject).Name},
			};
			rptS.ForEach(e => FillDefaultForMigration(e));
			context.Set<Rpt>().AddOrUpdate(rptS);
			Func<string, Rpt> getReport = (code) => rptS.First(r => r.Code == code);
			#endregion

			#region ���� � �������
			Role[] roleS = {
				new Role { Code = "Manager", Name = "��������" },
				new Role { Code = "CEO", Name = "��������" },
				new Role { Code = "Secretar", Name = "���������" },
				new Role { Code = "Admin", Name = "��������������" },
				new Role { Code = "Buh", Name = "����������" },
				new Role { Code = "Sbor", Name = "������� ������" },
				new Role { Code = "Razv", Name = "���������" },
			};
			roleS.ForEach(e => FillDefaultForMigration(e)); 
			context.RoleSet.AddOrUpdate(roleS);
			Func<string, Role> getRole = (code) => roleS.First(r => r.Code == code);

			//����� �� �������
			RoleDype[] rdS = {
				new RoleDype { Role = getRole("Manager"), Dype = getDype("SourceNews"), Access = AccessDype.READ },
				new RoleDype { Role = getRole("CEO"), Dype = getDype("SourceNews"), Access = AccessDype.EDIT },
				new RoleDype { Role = getRole("Sbor"), Dype = getDype("SourceNews"), Access = AccessDype.EDIT },
				new RoleDype { Role = getRole("Razv"), Dype = getDype("SourceNews"), Access = AccessDype.EDIT },
			};
			var rdAdm = dypeS.Select(dype => new RoleDype { Dype = dype, Role = getRole("Admin"), Access = AccessDype.ALL }).ToArray();
			rdS.ForEach(e => FillDefaultForMigration(e));
			rdAdm.ForEach(e => FillDefaultForMigration(e));
			rdAdm.Where(rd => rd.Dype.Code == "Role").ForEach(rd => rd.Content = "Read=Code smth".AddLine("Write=Name"));
			context.RoleDypeSet.AddOrUpdate(rdS);
			context.RoleDypeSet.AddOrUpdate(rdAdm);


			//������ � ��������
			RoleStatus[] rstS = {
				new RoleStatus { Role = getRole("Admin"), Status = getStatus("Testing")},
				new RoleStatus { Role = getRole("Admin"), Status = getStatus("Done")},
				new RoleStatus { Role = getRole("Admin"), Status = getStatus("DoneBad")},
			};
			rstS.ForEach(e => FillDefaultForMigration(e));
			context.Set<RoleStatus>().AddOrUpdate(rstS);

			//������ � ����� � ����������� �� �������
			RoleStatusContent[] rstcS = {
				new RoleStatusContent { Role = getRole("Admin"), Status = getStatus("Testing"), Content = "ReadNot=Login"}, //
				new RoleStatusContent { Role = getRole("Admin"), Status = getStatus("Done"), Content="WriteNot=Name Login"},
				new RoleStatusContent { Role = getRole("Admin"), Status = getStatus("DoneBad"), Content="Write=Name Login"},
			};
			rstcS.ForEach(e => FillDefaultForMigration(e));
			context.Set<RoleStatusContent>().AddOrUpdate(rstcS);

			//������ � �������
			RoleReport[] rrptS = {
				new RoleReport { Role = getRole("Admin"), Report = getReport("EcoEmplAbsentToday")},
				new RoleReport { Role = getRole("Admin"), Report = getReport("ProjectStat")},
			};
			rrptS.ForEach(e => FillDefaultForMigration(e));
			context.Set<RoleReport>().AddOrUpdate(rrptS);
			#endregion

			#region users
			//users
			User[] userS =
			{
				new User { Login = "Rulon", FirstName = "q", LastName = "q" },
				new User { Login = "UAdmin", FirstName = "��������", LastName = "���������" },
				new User { Login = "UBuh", FirstName = "���������", LastName = "�����������" },
				new User { Login = "USbor", FirstName = "����", LastName = "������" },
				new User { Login = "URazv", FirstName = "��������", LastName = "����������" },
			};
			userS.ForEach(e => FillDefaultForMigration(e));
			context.UserSet.AddOrUpdate(userS);
			Func<string, User> getUser = (login) => userS.First(r => r.Login == login);

			//��������� ������ � ����
			RoleUser[] ruS = {
				new RoleUser { User = getUser("Rulon"), Role = getRole("Manager") },
				new RoleUser { User = getUser("Rulon"), Role = getRole("CEO") },
				new RoleUser { User = getUser("UAdmin"), Role = getRole("Admin") },
				new RoleUser { User = getUser("UBuh"), Role = getRole("Buh") },
				new RoleUser { User = getUser("USbor"), Role = getRole("Sbor") },
				new RoleUser { User = getUser("URazv"), Role = getRole("Razv") },
			};
			ruS.ForEach(e => FillDefaultForMigration(e)); 
			context.Set<RoleUser>().AddOrUpdate(ruS);
			#endregion


			SeedEco(context);


		}//function

		void SeedEco(Osnova.Infrastructure.OsnovaDB context)
		{

			EcoCompany[] compS =
			{
				new EcoCompany() { Name="������", Code1c=11}
			};
			compS.ForEach(e => FillDefaultForMigration(e));
			context.EcoCompanySet.AddOrUpdate(compS);
			Func<string, EcoCompany> getCompany = (name) => compS.First(z => z.Name == name);

			EcoDepartment[] deptS = { 
				new EcoDepartment() { Code1c=22, Company=getCompany("������"), Name="����������" },
				new EcoDepartment() { Code1c=33, Company=getCompany("������"), Name="��" },
			};
			deptS.ForEach(e => FillDefaultForMigration(e));
			context.EcoDepartmentSet.AddOrUpdate(deptS);
			Func<string, EcoDepartment> getDept = (name) => deptS.First(z => z.Name == name);

			EcoEmplAbsentType[] emplatS = { 
													new EcoEmplAbsentType() {  Code="VACANCY", Name = "������"},
													new EcoEmplAbsentType() { Code="ILL",  Name="�������"}, 
													new EcoEmplAbsentType() { Code="ASK",  Name="�������"}, 
													new EcoEmplAbsentType() { Code="NO",  Name="��� ������"}, 
												};
			emplatS.ForEach(e => FillDefaultForMigration(e));
			context.EcoEmplAbsentTypeSet.AddOrUpdate(emplatS);
			Func<string, EcoEmplAbsentType> getEmplat = (code) => emplatS.First(z => z.Code == code);

			EcoEmpl[] emplS = { 
				new EcoEmpl() { Code1c=11, Department=getDept("����������"), Name="�������� �����", DtBeg= new DateTime(2000, 10,21), Email="es@mail.ru"},
				new EcoEmpl() { Code1c=22, Department=getDept("��"), Name="������ ����", DtBeg= new DateTime(2004, 1,17), Email="ii@mail.ru"}
			};
			emplS.ForEach(e => FillDefaultForMigration(e));
			context.EcoEmplSet.AddOrUpdate(emplS);
			Func<string, EcoEmpl> getEmpl = (name) => emplS.First(z => z.Name == name);

			try
			{
				System.IO.File.Delete(@"C:\osnova.log");
				context.Database.Log = (s) => System.IO.File.AppendAllText(@"C:\osnova.log", s);

			EcoEmplAbsent[] emplabsS = { 
				new EcoEmplAbsent() { DtAbsent = DateTime.Now, Empl=getEmpl("�������� �����"), Hours=8, EmplAbsentType=getEmplat("VACANCY"), Note="dudu"},
				new EcoEmplAbsent() { DtAbsent = DateTime.Now, Empl=getEmpl("������ ����"), Hours=3, EmplAbsentType=getEmplat("ILL"), Note="nana"},
			};
			emplabsS.ForEach(e => FillDefaultForMigration(e));
			
			context.EcoEmplAbsentSet.AddOrUpdate(emplabsS);
			//context.EcoEmplAbsentSet.AddRange(emplabsS);
			//context.SaveChanges();
			}//try
			catch (DbEntityValidationException exception2)
			{
				Action<string> log = (s) => System.IO.File.AppendAllText(@"C:\osnovaValidate.log", s);
				foreach (var item in exception2.EntityValidationErrors)
				{
					log("=========");
					foreach (var ve in item.ValidationErrors)
					{
						log(ve.PropertyName + " " + ve.ErrorMessage); 
					}//for
				}//for
				
			}//catch
			catch (Exception exception)
			{
				System.IO.File.AppendAllText(@"C:\osnovaAll.log", exception.ToString());
			}//catch
		}//function

	}//class
}//ns
