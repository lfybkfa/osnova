namespace Osnova.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EntityComment",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DypeId = c.Int(nullable: false),
                        EntityId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Dt = c.DateTime(nullable: false),
                        Content = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => new { t.DypeId, t.EntityId }, name: "IX_Comment");
            
            CreateTable(
                "dbo.EntityDype",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 250),
                        Name = c.String(nullable: false, maxLength: 250),
                        GroupdypeID = c.Int(nullable: false),
                        Dll = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EntityGroupdype", t => t.GroupdypeID)
                .Index(t => t.Code, unique: true, name: "IX_Dype_Code")
                .Index(t => t.Name, unique: true, name: "IX_Dype_Name")
                .Index(t => t.GroupdypeID);
            
            CreateTable(
                "dbo.EntityGroupdype",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 250),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.Code, unique: true, name: "IX_Groupdype_Code")
                .Index(t => t.Name, unique: true, name: "IX_Groupdype_Name");
            
            CreateTable(
                "dbo.EntityLock",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DypeId = c.Int(nullable: false),
                        EntityId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Dt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => new { t.DypeId, t.EntityId }, name: "IX_Lock");
            
            CreateTable(
                "dbo.JiraProject",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Note = c.String(nullable: false),
                        Lead = c.String(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EntityRoleDype",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RoleID = c.Int(nullable: false),
                        DypeID = c.Int(nullable: false),
                        Access = c.Int(nullable: false),
                        Content = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EntityDype", t => t.DypeID)
                .ForeignKey("dbo.EntityRole", t => t.RoleID)
                .Index(t => new { t.RoleID, t.DypeID }, unique: true, name: "IX_RoleDype");
            
            CreateTable(
                "dbo.EntityRole",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 250),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.Code, unique: true, name: "IX_Role_Code")
                .Index(t => t.Name, unique: true, name: "IX_Role_Name");
            
            CreateTable(
                "dbo.EntityRoleSpecial",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RoleID = c.Int(nullable: false),
                        SpecialID = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EntityRole", t => t.RoleID)
                .ForeignKey("dbo.EntitySpecial", t => t.SpecialID)
                .Index(t => new { t.RoleID, t.SpecialID }, unique: true, name: "IX_RoleSpecial");
            
            CreateTable(
                "dbo.EntitySpecial",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        DypeID = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EntityDype", t => t.DypeID)
                .Index(t => t.DypeID);
            
            CreateTable(
                "dbo.EntityRoleStatusContent",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RoleID = c.Int(nullable: false),
                        StatusID = c.Int(nullable: false),
                        Content = c.String(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EntityRole", t => t.RoleID)
                .ForeignKey("dbo.EntityStatus", t => t.StatusID)
                .Index(t => new { t.RoleID, t.StatusID }, unique: true, name: "IX_RoleStatusContent");
            
            CreateTable(
                "dbo.EntityStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 250),
                        Name = c.String(nullable: false, maxLength: 250),
                        DypeID = c.Int(nullable: false),
                        Content = c.String(),
                        Description = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EntityDype", t => t.DypeID)
                .Index(t => new { t.Code, t.DypeID }, unique: true, name: "IX_Status");
            
            CreateTable(
                "dbo.EntityRoleStatusRoute",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RoleID = c.Int(nullable: false),
                        StatusID = c.Int(nullable: false),
                        Content = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EntityRole", t => t.RoleID)
                .ForeignKey("dbo.EntityStatus", t => t.StatusID)
                .Index(t => new { t.RoleID, t.StatusID }, unique: true, name: "IX_RoleStatusRoute");
            
            CreateTable(
                "dbo.EntityRoleStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RoleID = c.Int(nullable: false),
                        StatusID = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EntityRole", t => t.RoleID)
                .ForeignKey("dbo.EntityStatus", t => t.StatusID)
                .Index(t => new { t.RoleID, t.StatusID }, unique: true, name: "IX_RoleStatus");
            
            CreateTable(
                "dbo.EntityRoleUser",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RoleID = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EntityRole", t => t.RoleID)
                .ForeignKey("dbo.EntityUser", t => t.UserID)
                .Index(t => new { t.RoleID, t.UserID }, unique: true, name: "IX_RoleUser");
            
            CreateTable(
                "dbo.EntityUser",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 250),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        TabelNum = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.Login, unique: true, name: "IX_User_Login");
            
            CreateTable(
                "dbo.EntitySourceNews",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Url = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        UserCreate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UserChange = c.Int(nullable: false),
                        ChangeDate = c.DateTime(nullable: false),
                        Stat = c.Int(),
                        UserResponsible = c.Int(),
                        UserFix = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EntityTicket",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DypeId = c.Int(nullable: false),
                        EntityId = c.Int(nullable: false),
                        UserId_Send = c.Int(nullable: false),
                        UserId_Recv = c.Int(nullable: false),
                        Stat = c.Int(nullable: false),
                        SendDate = c.DateTime(nullable: false),
                        RecvDate = c.DateTime(nullable: false),
                        IsActual = c.Boolean(nullable: false),
                        Content = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => new { t.DypeId, t.EntityId }, name: "IX_Ticket");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EntityRoleUser", "UserID", "dbo.EntityUser");
            DropForeignKey("dbo.EntityRoleUser", "RoleID", "dbo.EntityRole");
            DropForeignKey("dbo.EntityRoleStatus", "StatusID", "dbo.EntityStatus");
            DropForeignKey("dbo.EntityRoleStatus", "RoleID", "dbo.EntityRole");
            DropForeignKey("dbo.EntityRoleStatusRoute", "StatusID", "dbo.EntityStatus");
            DropForeignKey("dbo.EntityRoleStatusRoute", "RoleID", "dbo.EntityRole");
            DropForeignKey("dbo.EntityRoleStatusContent", "StatusID", "dbo.EntityStatus");
            DropForeignKey("dbo.EntityStatus", "DypeID", "dbo.EntityDype");
            DropForeignKey("dbo.EntityRoleStatusContent", "RoleID", "dbo.EntityRole");
            DropForeignKey("dbo.EntityRoleSpecial", "SpecialID", "dbo.EntitySpecial");
            DropForeignKey("dbo.EntitySpecial", "DypeID", "dbo.EntityDype");
            DropForeignKey("dbo.EntityRoleSpecial", "RoleID", "dbo.EntityRole");
            DropForeignKey("dbo.EntityRoleDype", "RoleID", "dbo.EntityRole");
            DropForeignKey("dbo.EntityRoleDype", "DypeID", "dbo.EntityDype");
            DropForeignKey("dbo.EntityDype", "GroupdypeID", "dbo.EntityGroupdype");
            DropIndex("dbo.EntityTicket", "IX_Ticket");
            DropIndex("dbo.EntityUser", "IX_User_Login");
            DropIndex("dbo.EntityRoleUser", "IX_RoleUser");
            DropIndex("dbo.EntityRoleStatus", "IX_RoleStatus");
            DropIndex("dbo.EntityRoleStatusRoute", "IX_RoleStatusRoute");
            DropIndex("dbo.EntityStatus", "IX_Status");
            DropIndex("dbo.EntityRoleStatusContent", "IX_RoleStatusContent");
            DropIndex("dbo.EntitySpecial", new[] { "DypeID" });
            DropIndex("dbo.EntityRoleSpecial", "IX_RoleSpecial");
            DropIndex("dbo.EntityRole", "IX_Role_Name");
            DropIndex("dbo.EntityRole", "IX_Role_Code");
            DropIndex("dbo.EntityRoleDype", "IX_RoleDype");
            DropIndex("dbo.EntityLock", "IX_Lock");
            DropIndex("dbo.EntityGroupdype", "IX_Groupdype_Name");
            DropIndex("dbo.EntityGroupdype", "IX_Groupdype_Code");
            DropIndex("dbo.EntityDype", new[] { "GroupdypeID" });
            DropIndex("dbo.EntityDype", "IX_Dype_Name");
            DropIndex("dbo.EntityDype", "IX_Dype_Code");
            DropIndex("dbo.EntityComment", "IX_Comment");
            DropTable("dbo.EntityTicket");
            DropTable("dbo.EntitySourceNews");
            DropTable("dbo.EntityUser");
            DropTable("dbo.EntityRoleUser");
            DropTable("dbo.EntityRoleStatus");
            DropTable("dbo.EntityRoleStatusRoute");
            DropTable("dbo.EntityStatus");
            DropTable("dbo.EntityRoleStatusContent");
            DropTable("dbo.EntitySpecial");
            DropTable("dbo.EntityRoleSpecial");
            DropTable("dbo.EntityRole");
            DropTable("dbo.EntityRoleDype");
            DropTable("dbo.JiraProject");
            DropTable("dbo.EntityLock");
            DropTable("dbo.EntityGroupdype");
            DropTable("dbo.EntityDype");
            DropTable("dbo.EntityComment");
        }
    }
}
