﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Infrastructure
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
	public sealed class FormatAttribute : Attribute
	{
		public FormatField Format;
		public string DypeName;
		public string LinkName;
		public string DisplayName;
		public DataTable DataSource;

		public FormatAttribute(FormatField format)
		{
			this.Format = format;
		}

		public FormatAttribute(FormatField format, string dypeName)
		{
			this.Format = format;
			this.DypeName = dypeName;
		}
		public FormatAttribute(FormatField format, string dypeName, string displayName)
		{
			this.Format = format;
			this.DypeName = dypeName;
			this.DisplayName = displayName;
		}


		public FormatAttribute(FormatField format, string dypeName, string linkName, string displayName)
		{
			this.Format = format;
			this.DypeName = dypeName;
			this.LinkName = linkName;
			this.DisplayName = displayName;
		}
	}
}