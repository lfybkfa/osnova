﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Infrastructure
{
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
	public class ParamAttribute : Attribute
	{
		public string Name { get; set; }
		public ParamAttribute(string Name)		{			this.Name = Name;		}//constructor
	}//class

	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
	public class ManyToManyAttribute : Attribute
	{
	}//class

	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public class AddableAttribute : Attribute
	{
	}//class

	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public class RemovableAttribute : Attribute
	{
	}//class

}//ns
