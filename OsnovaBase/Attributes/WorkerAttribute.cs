﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Infrastructure
{
	[AttributeUsage(AttributeTargets.Class)]
	public class WorkerAttribute : Attribute
	{
		/// <summary>
		/// статический класс, который обрабатывает Special
		/// </summary>
		public string Special;
		/// <summary>
		/// статический класс, который обрабатывает Move
		/// </summary>
		public string Move;
	}//class
}//ns
