﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;

namespace Osnova.Model
{
	public static class SourceNewsSpecial
	{
		private static Type MyType = typeof(SourceNews);

		[Param("Name")]
		public static bool Test(SC specialContext)
		{
			if (specialContext.entity != null)
				AC.Logger.Info("{0} {1}".Fmt(specialContext.entity.ToString(), specialContext.Name));
			else
				AC.Logger.Info("{0} {1}".Fmt(specialContext.type.Name, specialContext.Name));
			return true;
		}//function
	}//class
}//ns
