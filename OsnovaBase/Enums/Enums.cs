﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Infrastructure
{
    //Name_comp=bla
    public enum Comparision
    {
        Equal, NotEqual,
        GreaterThan, GreaterThanOrEqual,
        LessThan, LessThanOrEqual,
        CONTAINS, BEGIN, END,
        IN, IsNULL,
        CurrentDATE, CurrentMONTH, CurrentYEAR, DATE, MONTH, YEAR,
        OR
    }//enum


    [Flags]
    public enum AccessDype
    {
        READ = 1,
        WRITE = 2,
        FIX = 4, //фиксация Idea=d150212,
        EDIT = READ | WRITE, NONE = 0, ALL = READ | WRITE | FIX,
        ADD = 8,
        DELETE=16,
        MENU=32
       


    }//enum

    public enum CollectMode
    {
        Manual = 1,
        Auto = 2,
        Gateway = 3
    }

    public enum FormatField
    {
        Combo,
        Grid,
        GridForm,
        Link,
        ImgCombo,
        Icon,
        CheckIcon,
        MultiCombo,
        Hidden,
        File,
        Color,
        Numeric,
        LargeCombo,
        FieldMultiCombo,
        AccessMultiCombo,
        Time,
        FileAttached

    }//enum

    public enum CardType
    {
        //Добавление нового документа
        CardAdd = 0,
        //Изменение документа
        Card = 1,
        //Только для чтения
        OnlyRead = 2
    }

    public enum SourceEventType
    {
        /// <summary>
        /// Нет события
        /// </summary>
        Null = -1,

        /// <summary>
        /// Создан источник 
        /// </summary>
        SourceCreated = 1,

        /// <summary>
        /// Отзыв
        /// </summary>
        Cancel = 2,


        /// <summary>
        /// Отзыв ТП
        /// </summary>
        CancelSupport = 3,

        /// <summary>
        /// Ошибка сбора
        /// </summary
        ErrorCollection = 4,

        /// <summary>
        /// Ошибка сбора для ТП
        /// </summary
        ErrorCollectionSupport = 5,

        /// <summary>
        /// Уточнение сбора
        /// </summary
        SourceUpdate = 6,

        /// <summary>
        /// Передан на сбор
        /// </summary>
        ReferToCollection = 7,

        /// <summary>
        /// Принят к сбору
        /// </summary>
        AcceptToCollection = 8,

        /// <summary>
        /// Сбор данных
        /// </summary>
        DataCollection = 9,

        /// <summary>
        /// На исправлении
        /// </summary>
        OnFixed = 10,

        /// <summary>
        /// Изменен
        /// </summary>
        Changed = 11,

        /// <summary>
        /// Слияние
        /// </summary>
        Merged = 12,

        /// <summary>
        /// Юридическая проверка
        /// </summary>
        LegalReview = 13
    }

}//ns
