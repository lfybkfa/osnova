﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Infrastructure
{
	public enum ErrorCode
	{
		CONDITION_NOT_PARSED,
		CONDITION_NOT_SET,
		CONVERSION_ERROR,
		ENTITY_FIX_ERROR,
		ENTITY_IS_FIXED,
		ENTITY_IS_LOCK,
		ENTITY_IS_NULL,
		ENTITY_NOT_DELETED,
		ENTITY_NOT_FOUND,
		ENTITY_NOT_LOADED,
		ENTITY_NOT_SAVED,
		ENTITY_NOT_VALIDATED,
		LINK_NOT_ADDED,
		LINK_NOT_LOADED, //список не загружен
		LINK_NOT_REMOVED,
		LINK_TYPE_NOT_FOUND, //предлагаемый тип не link
		METHOD_CHECK_NOT_FOUND,
		METHOD_LINK_NOT_FOUND, //не найден метод, соответствующий линку
		METHOD_RUN_NOT_FOUND,
		METHOD_SPECIAL_NOT_FOUND,
		MOVE_ERROR,
		MOVE_NOT_CHECKED,
		MOVE_NOT_RUN,
		SPECIAL_NOT_RUN,
		STATUS_NOT_UPDATED,
		STATUS_FROM_BAD, // неверны статус-источник
		STATUS_READONLY, // статус только для чтения
		STATUS_TO_NOT_FOUND,
		TYPE_NOT_ENTITY, //предлагаемый тип не entity
		TYPE_NOT_WORKER,
		USER_NOT_FIX_ACCESS,
		USER_NOT_LOGINED,
		USER_NOT_READ_ACCESS,
		USER_NOT_MOVE_ACCESS, //юзер не имеет права двигать в данный статус
		USER_NOT_RUN_ACCESS,
		USER_NOT_SET,
		USER_NOT_WRITE_ACCESS,
		VALIDATION_NOT_FILLED,
		VALIDATION_NOT_UNIQ,
		VALIDATION_NOT_PASSED,
		REPORT_NOT_SET,
		REPORT_NOT_EXIST,
		USER_NOT_REPORT_ACCESS,
		REPORT_NOT_CLASS,
		REPORT_INSTANCE_ERROR,
		REPORT_NOT_RUN,
	}//enum

}//ns
