﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova
{
	public static class MaybeExtension
	{
		/// <summary> 
		/// Позволяет работать с объектом <paramref name="source"/> , даже если он null
		/// Если null, то возвращает default(<typeparamref name="TResult"/>)
		/// Если не null, то возвращает результат функции(<paramref name="action"/>)
		/// </summary> 
		/// <typeparam name="TSource">Тип объекта</typeparam> 
		/// <typeparam name="TResult">Тип результата</typeparam> 
		/// <param name="source">Объект</param> 
		/// <param name="action">Функция, дающая результат</param> 
		/// <returns>результат</returns> 
		public static TResult With<TSource, TResult>(this TSource source, Func<TSource, TResult> action)
		where TSource : class
		{
			if (source != default(TSource)) 	{				return action(source);			}//if
			else		{		return default(TResult);		}//else
		}//function

		public static TResult With<TSource, TResult>(this TSource source, Func<TSource, TResult> action, TResult defaultValue)
		where TSource : class
		{
			if (source != default(TSource))		{		return action(source);		}//if
			else	{		return defaultValue;		}//else
		}//function

		public static TSource Do<TSource>(this TSource source, Action<TSource> action)	where TSource : class		{	if (source != default(TSource))	{action(source);}		return source; }//function

		/// <summary>
		/// вертает сам объект, если он не пустой и выполнено условие condition
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <param name="source"></param>
		/// <param name="condition"></param>
		/// <returns></returns>
		public static TSource If<TSource>(this TSource source, Func<TSource, bool> condition)
		where TSource : class
		{
			if ((source != default(TSource)) && (condition(source) == true))
			{
				return source;
			}//if
			else
			{
				return default(TSource);
			}//else
		}//function

		//public static TInput Recover<TInput>(this TInput source, Func<TInput> action)		where TInput : class		{			return source ?? action();		}//function
	}//class
}//ns
