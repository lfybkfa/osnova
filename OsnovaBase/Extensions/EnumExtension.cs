﻿using Osnova.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova
{
	public static class EnumExtension
	{
		public static AccessDype Set(this AccessDype en, AccessDype value) { return en |= value; }//function
		public static AccessDype Clear(this AccessDype en, AccessDype value) { return en &= ~value; }//function
	}//class
}//ns
