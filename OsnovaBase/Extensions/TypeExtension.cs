﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;

namespace Osnova
{
	public static class TypeExtension
	{
		public static bool IsNullable(this Type t)
		{
			if (t.IsGenericType)
				return t.GetGenericTypeDefinition() == typeof(Nullable<>);
			else
				return false;
		}//function

		public static Type GetTypeUnderNullable(this Type t)
		{
			return t.IsNullable() ? Nullable.GetUnderlyingType(t) : t;
		}//function

		/// <summary>
		/// считывает поля у любого класса по следующим критериям
		/// - value type или string
		/// - не отмечен атрибутом NotMapped
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static IEnumerable<Field> GetFieldsDype(this Type type)
		{
			return type.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public)
				.Where(p => p.GetCustomAttributes(typeof(NotMappedAttribute), true).Any() == false)
				.Where(p => p.PropertyType.IsClass == false || p.PropertyType == typeof(string))
				.Select(p => new Field
				{
					property = p,
					IsNullable = p.PropertyType.IsNullable(),
					display = p.GetCustomAttribute<DisplayAttribute>(),
					format = p.GetCustomAttribute<FormatAttribute>(),
				});
		}//function

		public static WorkerAttribute GetWorker(this Type t)
		{
			return t.GetCustomAttribute<WorkerAttribute>();
		}//function
	}//class
}//ns
