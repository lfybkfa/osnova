﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova
{
	public static class IEnumerableExtension
	{
		public static IEnumerable<T> ForEach<T>(this IEnumerable<T> source, Action<T> action, bool IgnoreNull = true)
		{
			if (source == null)
				return null;

			if (action == null)
				return source;

			foreach (var item in source)
			{
				if (IgnoreNull && item == null) { continue; }
				action(item);
			}//for

			return source;
		}//function

		public static TResult[] SelectToArray<T, TResult>(this ICollection<T> source, Func<T, TResult> func)
		{
			if (source == null)
				throw new ArgumentNullException("source");
			TResult[] result = new TResult[source.Count];
			int i = 0;
			foreach (var value in source)
			{
				result[i++] = func(value);
			}
			return result;
		}//function
	}//class
}//ns
