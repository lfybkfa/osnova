﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Osnova
{
	public static class ExpressionExtension
	{
		public static Expression Between(this Expression Xprop, object beg, object end)
		{
			if (Xprop.NodeType != ExpressionType.MemberAccess && beg == null && end == null)
				return null;

			var Xbeg = Expression.Constant(beg);
			var Xend = Expression.Constant(end);
			return Expression.And(
				Expression.GreaterThanOrEqual(Xprop, Xbeg)
				, Expression.LessThan(Xprop, Xend));
		}//function
	}//class
}//ns
