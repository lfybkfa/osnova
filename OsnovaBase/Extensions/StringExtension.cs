﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;

namespace Osnova
{
	public static class StringExtension
	{
		public static string Fmt(this string format, params object[] args)	{	return string.Format(format, args);	}//function
		public static bool IsNothing(this string s) { return string.IsNullOrWhiteSpace(s); }//function
		public static string[] Lines(this string s) { return s.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries); }//function
		
		/// <summary>
		/// конкатенация строк через StringBuilder
		/// </summary>
		/// <param name="s"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static string Add(this string s, params object[] args)
		{
			StringBuilder sb = new StringBuilder(s);
			args.ForEach(o => sb.Append(o)); 
			return sb.ToString(); 
		}//function

		public static string AddLine(this string s, params object[] args)
		{
			StringBuilder sb = new StringBuilder(s);
			args.ForEach(o => sb.AppendFormat("{0}{1}", Environment.NewLine, o));
			return sb.ToString();
		}//function

		/// <summary>
		/// достает Value из строки вида Key=Value
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		public static string IniValue(this string s)
		{
			return s.Substring(s.IndexOf(CHAR.EQ) + 1);
		}//function

		/// <summary>
		/// достает Key из строки вида Key=Value или Key
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		public static string IniKey(this string s)
		{
			int i = s.IndexOf(CHAR.EQ);
			return (i < 0) ? s : s.Substring(0, i);
		}//function

	}//class

	
}//ns
