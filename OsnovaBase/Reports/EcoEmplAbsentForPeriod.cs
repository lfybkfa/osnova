﻿using Osnova.Infrastructure;
using Osnova.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Reports
{
	public class EcoEmplAbsentForPeriod: IReportRunner
	{
		public DataSet Run(RC reportContext)
		{
			#region dataset
			DataSet ds = new DataSet();
			ds.Tables.Add(new DataTable("Отсутствие сотрудников"));
			#endregion

			#region params
			//значения по умолчанию
			DateTime dtStart = DateTime.Now.Date;
			DateTime dtEnd = DateTime.Now.Date;

			//значения из параметров, если параметры заполнены
			dtStart = reportContext.ParamGet(WORD.Start).With(o => (DateTime)o, dtStart);
			dtEnd = reportContext.ParamGet(WORD.End).With(o => (DateTime)o, dtEnd);

			//свап параметров если не соблюден порядок
			if (dtStart > dtEnd)
			{
				DateTime dtSwap = dtStart; 
				dtStart = dtEnd; dtEnd = dtSwap;
			}//if
			#endregion

			LC lc = new LC();
			lc.AddCondition(new Condition() { comp = Comparision.GreaterThanOrEqual, Name = "DtAbsent", Value=dtStart.ToString() });
			lc.AddCondition(new Condition() { comp = Comparision.LessThanOrEqual, Name = "DtAbsent", Value = dtEnd.ToString() });
			bool OK = lc.SelectAll<Model.EcoEmplAbsent>();
			if (!OK)
			{
				reportContext.err = lc.err;
			}//if

			return ds;
		}
	}//class
}//ns
