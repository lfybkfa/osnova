﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;

namespace Osnova.Model
{
	public partial class Comment
	{
		[Required]
		public int ID { get; set; }

		[Required]
		[Index("IX_Comment", IsUnique = false, Order = 1)]
		public int DypeId { get; set; }

		[Required]
		[Index("IX_Comment", IsUnique = false, Order = 2)]
		public int EntityId { get; set; }

		[Required]
		public int UserId { get; set; }

		[Required]
		public DateTime Dt { get; set; }

		[Required]
		public String Content { get; set; }

		[NotMapped]
		public string Table { get { return OsnovaDB.GetTableName<Comment>(); } }

		public bool Save()
		{
			using (var ctx = OsnovaDB.New)
			{
				Dt = DateTime.Now;
				ctx.CommentSet.Add(this);
				ctx.SaveChanges();
			}//using
			return true;
		}//function

		public static Comment CreateForEntity(EntityBase entity)
		{
			Comment result = new Comment { DypeId = entity.MetaDype.ID, EntityId = entity.ID };
			return result;
		}//function

		public static IEnumerable<Comment> GetList(EntityBase entity)
		{
			Dype dype = AC.Meta.GetDype(entity.GetType());
			if (dype == null) { return null; }

			IEnumerable<Comment> result = null;
			using (var context = OsnovaDB.New)
			{
				var query = context.CommentSet.AsNoTracking().Where(o => o.DypeId == dype.ID && o.EntityId == entity.ID);
				result = query.ToArray();
			}//using
			return result;
		}//function

		public override string ToString()
		{
			return "UserId=".Add(UserId, " Dt=", Dt, Content);
		}//function
	}//class
}//ns
