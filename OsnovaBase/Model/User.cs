﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;

namespace Osnova.Model
{
	[Worker(Move = "UserOther")]
	public partial class User: EntityBase
	{
		[Required(AllowEmptyStrings = false)]
		[MaxLength(250)]
		[Index("IX_User_Login", IsUnique=true)]
		public string Login { get; set; }

		[Required(AllowEmptyStrings = false)]		
		public string FirstName { get; set; }
		
		[Required(AllowEmptyStrings = false)]		
		public string LastName { get; set; }

		public string TabelNum { get; set; }

		[NotMapped]
		public string FIO		{	get { return "{0} {1}".Fmt(FirstName, LastName); } }

		public override string ToString() { return "{0}. Login={1}".Fmt(base.ToString(), Login); }

		public static User GetOnLogin(string Login)
		{
			User result = null;
			using (var ctx = OsnovaDB.New)
			{
				result = ctx.Set<User>().AsNoTracking().FirstOrDefault(u => u.Login == Login);
			}//using
			return result;
		}//function

	}//class
}//ns
