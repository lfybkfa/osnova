﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;

namespace Osnova.Model
{
	public partial class SourceNews: EntityBase
	{
		public string Name { get; set; }
		public string Url { get; set; }

	}
}
