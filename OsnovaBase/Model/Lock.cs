﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;

namespace Osnova.Model
{
	/// <summary>
	/// сервисный объект для функционала захвата
	/// Ides=d120315
	/// </summary>
	public partial class Lock
	{
		[Required]
		public int ID { get; set; }

		[Required]
		[Index("IX_Lock", IsUnique = false, Order = 1)]
		public int DypeId { get; set; }

		[Required]
		[Index("IX_Lock", IsUnique = false, Order = 2)]
		public int EntityId { get; set; }

		[Required]
		public int UserId { get; set; }

		[Required]
		public DateTime Dt { get; set; }

		[NotMapped]
		public string Table { get { return OsnovaDB.GetTableName<Lock>(); } }

		public Lock()
		{
			Dt = DateTime.Now;
		}//constructor

		/// <summary>
		/// захватить объект на себя
		/// </summary>
		/// <returns></returns>
		public bool Save()
		{
			using (var ctx = OsnovaDB.New)
			{
				ctx.LockSet.Add(this);
				ctx.SaveChanges();
			}//using
			return true;
		}//function

		/// <summary>
		/// удалить все захваты с объекта
		/// </summary>
		/// <returns></returns>
		public bool Delete()
		{
			using (var ctx = OsnovaDB.New)
			{
				string sql = "DELETE FROM ".Add(Table, " WHERE DypeId = ", DypeId, " AND EntityId = ", EntityId);
				ctx.RunSQL(sql);
			}//using
			return true;
		}//function

		public bool Refresh()
		{
			using (var ctx = OsnovaDB.New)
			{
				string sql = "UPDATE ".Add(Table, " SET Dt = getdate() WHERE DypeId = ", DypeId, " AND EntityId = ", EntityId, " AND UserId = ", UserId);
				ctx.RunSQL(sql);
			}//using
			return true;
		}//function

		public static Lock Find(Lock sample)
		{
			Lock result = null;
			using (var ctx = OsnovaDB.New)
			{
				var query = ctx.Set<Lock>().AsNoTracking().Where(o => o.DypeId == sample.DypeId && o.EntityId == sample.EntityId);
				result = query.FirstOrDefault();
			}//using
			return result;
		}//function

		public override string ToString()
		{
			return "IS LOCKED BY {0} {1}".Fmt(UserId, Dt.ToString());
		}//function
	}//class
}//ns
