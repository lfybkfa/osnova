﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;

namespace Osnova.Model
{
	[ManyToMany]
	public partial class RoleDype: EntityBase
	{
		[Required]
		[Index("IX_RoleDype", IsUnique = true, Order = 1)]
		public int RoleID { get; set; }

		[ForeignKey("RoleID")]
		public Role Role { get; set; }

		[Required]
		[Index("IX_RoleDype", IsUnique = true, Order = 2)]
		public int DypeID { get; set; }

		[ForeignKey("DypeID")]
		public Dype Dype { get; set; }

		[Required]
		public AccessDype Access { get; set; }

		public string Content { get; set; }

		public override IEnumerable<Err> Validate()
		{
			List<Err> result = null;
			if (Access == AccessDype.NONE)
			{
				result = result ?? new List<Err>();
				result.Add(new Err { errorCode = ErrorCode.VALIDATION_NOT_FILLED, message = "Access is not set" });
				return result;
			}//if
			return base.Validate();
		}//function
	}//class
}//ns
