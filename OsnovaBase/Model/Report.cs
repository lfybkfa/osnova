﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Model
{
	public partial class Report: EntityBase
	{
		[Required(AllowEmptyStrings = false)]
		[Index("IX_Report_Code", IsUnique = true)]
		[MaxLength(250)]
		[Display(Name = "Код")]
		public string Code { get; set; }

		[Required(AllowEmptyStrings = false)]
		[Index("IX_Report_Name", IsUnique = true)]
		[MaxLength(250)]
		[Display(Description = "Наименование для меню")]
		public string Name { get; set; }

		[Required]
		[Display(Description = "Информация о цели отчета, алгоритме, заказчике")]
		public string Description { get; set; }

		[Display(Description = "Параметры")]
		public string Parameters { get; set; }

		[Display(Description = "Если заполнено, то отчет является печатной формой данного Dype")]
		public string DypeCode { get; set; }

		[NotMapped]
		[Display(Description = "Печатная форма для карточки?")]
		public bool IsPrintForm { get { return DypeCode.IsNothing(); } }

		[NotMapped]
		[Display(Description = "Есть ли параметры?")]
		public bool HasParameters { get { return Parameters.IsNothing(); } }

	}
}
