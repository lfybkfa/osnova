﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Model
{
	/// <summary>
	/// разрешает перезод данной роли в данный статус
	/// </summary>
	public partial class RoleStatus: EntityBase
	{
		[Required]
		[Index("IX_RoleStatus", IsUnique = true, Order = 1)]
		public int RoleID { get; set; }
		public Role Role { get; set; }

		[Required]
		[Index("IX_RoleStatus", IsUnique = true, Order = 2)]
		public int StatusID { get; set; }
		public Status Status { get; set; }

	}//class
}//ns
