﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;

namespace Osnova.Model
{
	public class Ticket
	{
		[Required]
		public int ID { get; set; }

		[Required]
		[Index("IX_Ticket", IsUnique = false, Order = 1)]
		public int DypeId { get; set; }

		[Required]
		[Index("IX_Ticket", IsUnique = false, Order = 2)]
		public int EntityId { get; set; }

		[Required]
		public int UserId_Send { get; set; }

		[Required]
		public int UserId_Recv { get; set; }

		[Required]
		public int Stat { get; set; }

		[Required]
		public DateTime SendDate { get; set; }

		[Required]
		public DateTime RecvDate { get; set; }

		[Required]
		public bool IsActual { get; set; }

		[Required]
		public String Content { get; set; }

		[NotMapped]
		public string Table { get { return OsnovaDB.GetTableName<Ticket>(); } }

	}//class
}//ns
