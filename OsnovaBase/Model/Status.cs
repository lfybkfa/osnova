﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;

namespace Osnova.Model
{
	public partial class Status: EntityBase
	{
		[Required(AllowEmptyStrings = false)]
		[Index("IX_Status", IsUnique = true, Order=1)]
		[MaxLength(250)]
		public string Code { get; set; }

		[Required(AllowEmptyStrings = false)]
		[MaxLength(250)]
		public string Name { get; set; }

		[Required]
		[Index("IX_Status", IsUnique = true, Order = 2)]
		public int DypeID { get; set; }
		public Dype Dype { get; set; }

		/// <summary>
		/// поле для хранения настроек
		/// </summary>
		public string Content { get; set; }

		/// <summary>
		/// текстовое описание статуса
		/// показывается пользователю
		/// </summary>
		public string Description { get; set; }

		public override string ToString() { return "{0}. Code={1}. Name={2}".Fmt(base.ToString(), Code, Name); }

		[NotMapped]
		internal string[] FromS;
		[NotMapped]
		internal bool IsFirst { get { return FromS == null || !FromS.Any() || FromS.Contains(WORD.NULL); } }
		[NotMapped]
		internal bool IsReadonly = false;
		[NotMapped]
		internal string[] CheckS;
		[NotMapped]
		internal bool HasCheck { get { return CheckS != null && CheckS.Any(); } }
		[NotMapped]
		internal string[] RunS;
		[NotMapped]
		internal bool HasRun { get { return RunS != null && RunS.Any(); } }
		[NotMapped]
		internal bool IsLast;

		public void Prepare()
		{
			if (Content.IsNothing())	{	return;	}//if
			string[] ss = Content.Lines();
			#region Parse
			foreach (var s in ss)
			{
				if (false) { ; }//if
				else if (s.StartsWith(WORD.Last)) { IsLast = true; }//if
				else if (s.StartsWith(WORD.Readonly)) { IsReadonly = true; }//if
				else if (s.StartsWith(WORD.Check)) { CheckS = s.IniValue().Split(CHAR.DELIM); }//if
				else if (s.StartsWith(WORD.Run)) { RunS = s.IniValue().Split(CHAR.DELIM); }//if
				else if (s.StartsWith(WORD.From)) { FromS = s.IniValue().Split(CHAR.DELIM); }//if
			}//for

			CheckS = CheckS ?? STR.EMPTY;
			RunS = RunS ?? STR.EMPTY;
			FromS = FromS ?? STR.EMPTY;
			#endregion
			Content = null;
		}//function
	}//class
}//ns
