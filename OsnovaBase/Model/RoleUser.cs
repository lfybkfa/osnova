﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;

namespace Osnova.Model
{
	[ManyToMany]
	public partial class RoleUser : EntityBase
	{
		[Required]
		[Index("IX_RoleUser", IsUnique = true, Order = 1)]
		public int RoleID { get; set; }

		[Required]
		[Index("IX_RoleUser", IsUnique = true, Order = 2)]
		public int UserID { get; set; }

		public User User { get; set; }
		public Role Role { get; set; }
	}//class
}//ns
