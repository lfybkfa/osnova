﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Model
{
	public class Special: EntityBase
	{
		[Required(AllowEmptyStrings = false)]
		public string Code { get; set; }
		[Required(AllowEmptyStrings = false)]
		public string Name { get; set; }
		[Required]
		public int DypeID { get; set; }

		public Dype Dype { get; set; }

		public override string ToString() { return "{0}. Code={1}. Name={2}".Fmt(base.ToString(), Code, Name); }

	}//class
}//ns
