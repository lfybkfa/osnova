﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Model
{
	public class RoleReport : EntityBase
	{
		[Required]
		[Index("IX_RoleReport", IsUnique = true, Order = 1)]
		public int RoleID { get; set; }

		[Required]
		[Index("IX_RoleReport", IsUnique = true, Order = 2)]
		public int ReportID { get; set; }

		public Role Role { get; set; }
		public Report Report { get; set; }
	}//class
}//ns
