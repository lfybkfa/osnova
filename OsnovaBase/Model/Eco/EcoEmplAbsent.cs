﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Osnova.Model
{
	//СотрудникОтсутствие
	public class EcoEmplAbsent : EntityBase
	{
		#region properties
		[Required]
		[Display(Description = "Сотрудник")]
		public int EmplID { get; set; }
		public EcoEmpl Empl { get; set; }

		[Required]
		[Display(Description = "Дата отсутствия")]
		[DataType(DataType.Date)]
		public DateTime DtAbsent { get; set; }

		[Required]
		[Display(Description="Часы")]
		[Range(1, 8)]
		public int Hours { get; set; }

		[Required]
		[Display(Description = "Причина")]
		public int EmplAbsentTypeID { get; set; }
		public EcoEmplAbsentType EmplAbsentType { get; set; }

		[Required]
		[Display(Description = "Примечание")]
		public string Note { get; set; }

		#endregion
	}//class
}//namespace
