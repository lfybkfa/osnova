﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Osnova.Model
{
	public class EcoActivity : EntityBase
	{
		[Required]
		public string Code { get; set; }

		[Required]
		public string Name { get; set; }
	}//class
}//namespace
