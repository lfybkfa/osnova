﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Osnova.Model
{
	public class EcoWorklog : EntityBase
	{
		[Required]
		public int IssueID { get; set; }
		public EcoIssue Issue { get; set; }

		[Required]
		[DataType(DataType.Date)]
		public DateTime DtWork { get; set; }

		[Required]
		[Range(1, 8)]
		public int Hours { get; set; }

		[Required]
		public int Seconds { get; set; }

		[Required]
		public string Author { get; set; }

	}//class
}//namespace
