﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Osnova.Model
{
	public class EcoEmplOver : EntityBase
	{
		#region properties
		[Required]
		[Display(Description = "Сотрудник")]
		public int EmplID { get; set; }
		public EcoEmpl EcoEmpl { get; set; }

		[Required]
		[Display(Description = "Дата переработки")]
		[DataType(DataType.Date)]
		public DateTime DtOver { get; set; }

		[Required]
		[Display(Description = "Часы")]
		[Range(1, 8)]
		public int Hours { get; set; }

		[Required]
		[Display(Description = "Причина")]
		public int EmplOverTypeID { get; set; }
		public EcoEmplOverType EmplOverType { get; set; }

		[Required]
		[Display(Description = "Примечание")]
		public string Note { get; set; }

		#endregion
	}//class
}//namespace
