﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Osnova.Infrastructure;

namespace Osnova.Model
{
	//Сотрудник
	public class EcoEmpl: EntityBase
	{
		#region properties
		[Required]
		[MaxLength(250)]
		//[Index("IX_Empl_Name", IsUnique = false)]
		public string Name { get; set; }

		[Required]
		[DataType(DataType.Date)]
		public DateTime DtBeg { get; set; }

		[Required]
		[DataType(DataType.Date)]
		public DateTime DtEnd { get; set; }

		[Required]
		public int Code1c { get; set; }

		[Required]
		[EmailAddress]
		public string Email { get; set; }

		public string Jira { get; set; }

		[Required]
		public int DepartmentID { get; set; }
		public EcoDepartment Department { get; set; }

		[Required]
		[Display(Description="Часы")]
		public int Hours { get; set; }
		#endregion

		public EcoEmpl()
		{
			DtEnd = DateTime.MaxValue;
			Hours = 8;
			Jira = null;
		}//constructor

		public override IEnumerable<Err> Validate()
		{
			HashSet<Err> result = null;
			if (DtBeg >= DtEnd)
			{
				result = result ?? new HashSet<Err>();
				result.Add(new Err { errorCode = ErrorCode.VALIDATION_NOT_PASSED, message = "Дата прием больше даты увольнения" });
			}//if
			return result;
		}
	}//class
}//namespace
