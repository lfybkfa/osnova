﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Osnova.Model
{
	//Подразделение
	public class EcoDepartment : EntityBase
	{
		#region properties
		[Required]
		public int Code1c { get; set; }

		[Required]
		public string Name { get; set; }

		[Required]
		public int CompanyID { get; set; }

		[ForeignKey("CompanyID")]
		public EcoCompany Company { get; set; }
		#endregion
	}//class
}//namespace
