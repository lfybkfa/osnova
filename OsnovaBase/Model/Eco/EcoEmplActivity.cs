﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Osnova.Model
{
	public class EcoEmplActivity : EntityBase
	{
		[Required]
		[Display(Description = "Сотрудник")]
		public int EmplID { get; set; }
		public EcoEmpl EcoEmpl { get; set; }

		[Required]
		[Display(Description = "Активность")]
		public int ActivityID { get; set; }
		public EcoActivity Activity { get; set; }

		[Required]
		[Display(Description = "Часы")]
		public int Hours { get; set; }
	}//class
}//namespace
