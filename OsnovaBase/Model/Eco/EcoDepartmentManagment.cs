﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Osnova.Model
{
	//ПодразделениеМенеджмент
	public class EcoDepartmentManagment : EntityBase
	{
		#region properties
		[Required]
		public int DepartmentID { get; set; }
		public EcoDepartment Department { get; set; }

		[Required]
		public int EmplID { get; set; }
		public EcoEmpl EcoEmpl { get; set; }
		#endregion

	}//class
}//namespace
