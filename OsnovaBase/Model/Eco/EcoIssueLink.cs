﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Osnova.Model
{
	/// <summary>
	/// Связь между задачами.
	/// Забирается из Jira через view JiraIssueLink
	/// </summary>
	public class EcoIssueLink : EntityBase
	{
		[Required]
		public int ParentID { get; set; }
		public EcoIssue Parent { get; set; }

		[Required]
		public int IssueID { get; set; }
		public EcoIssue Issue { get; set; }

		[Required]
		public string LinkType { get; set; }
	}//class
}//namespace
