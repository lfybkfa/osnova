﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Osnova.Model
{
	public class EcoProject : EntityBase
	{
		[Required]
		[Display(Description = "Код")]
		public string Code { get; set; }

		[Required]
		[Display(Description = "Наименование")]
		public string Name { get; set; }

		[Required]
		[Display(Description = "Описание")]
		public string Note { get; set; }

		[Required]
		[Display(Description = "Менеджер проекта")]
		public string Lead { get; set; }
	}//class
}//namespace
