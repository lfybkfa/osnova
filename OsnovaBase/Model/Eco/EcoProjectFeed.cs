﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Osnova.Model
{
	/// <summary>
	/// Источник финансирования для проектов
	/// Если проект есть среди CodeParent, но нет среди Code = внешний проект = сам кормит, а его нет
	/// Если проект есть среди CodeParent и среди Code = внутренний проект (в сумме (SUM(Part)) кормильщиков должно быть 100%)
	/// Если проект нет среди CodeParent и среди Code = базовый проект = его по умолчанию кормят все
	/// </summary>
	public class EcoProjectFeed : EntityBase
	{
		[Required]
		public string Code { get; set; }

		[Required]
		public string CodeParent { get; set; }

		[Required]
		public int Part { get; set; }
	}//class
}//namespace
