﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Osnova.Model
{
	/// <summary>
	/// Задача.
	/// Не живет в БД
	/// Данные лежат в Jira и запрашиваются через view JiraIssue
	/// </summary>
	public class EcoIssue : EntityBase
	{
		[Required]
		[Display(Description = "Проект")]
		public int ProjectID { get; set; }
		public EcoProject Project { get; set; }

		[Required]
		[Display(Description = "Код")]
		public string Code { get; set; }

		[Required]
		[Display(Description = "Наименование")]
		public string Name { get; set; }

		[Required]
		[Display(Description = "Описание")]
		public string Note { get; set; }

		[Required]
		[Display(Description = "Автор задачи")]
		public string Reporter { get; set; }

		[Required]
		[Display(Description = "Исполнитель")]
		public string Assignee { get; set; }

		[Required]
		[Display(Description = "Тип задачи")]
		public string IssueType { get; set; }

		[Required]
		[Display(Description = "Статус задачи")]
		public string IssueStatus { get; set; }

	}//class
}//namespace
