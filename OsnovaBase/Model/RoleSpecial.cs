﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Model
{
	public class RoleSpecial : EntityBase
	{
		[Required]
		[Index("IX_RoleSpecial", IsUnique = true, Order = 1)]
		public int RoleID { get; set; }

		[Required]
		[Index("IX_RoleSpecial", IsUnique = true, Order = 2)]
		public int SpecialID { get; set; }

		public Role Role { get; set; }
		public Special Special { get; set; }
	}//class
}//ns
