﻿using Osnova.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Model
{
	public partial class Dype: EntityBase
	{
		[Required(AllowEmptyStrings = false)]
		[Index("IX_Dype_Code", IsUnique=true)]
		[MaxLength(250)]
		[Display(Name="Код")]
		public string Code { get; set; }
		
		[Required(AllowEmptyStrings = false)]
		[Index("IX_Dype_Name", IsUnique = true)]
		[MaxLength(250)]
		public string Name { get; set; }
		
		[Required]
		public int GroupdypeID { get; set; }

		[ForeignKey("GroupdypeID")]
		public Groupdype Groupdype { get; set; }

		/// <summary>
		/// assembly в которой располагается объект
		/// если не указано, то в основной
		/// </summary>
		public string Dll { get; set; }

		public override string ToString() { return "{0}. Code={1}. Name={2}".Fmt(base.ToString(), Code, Name); }


		[NotMapped]
		public IEnumerable<Field> FieldsAll { get { return fieldsAll; } }
		private Field[] fieldsAll;

		[NotMapped]
		public bool HasStatus {get; internal set;}

		internal void Prepare()
		{
			Type type = EntityBase.GetType(Code);
			if (type == null) { return; }

			var fields = AC.Meta.BaseFields;
			fields = fields.Concat(Field.Get(type));
			fieldsAll = fields.OrderBy(f => f.Order).ToArray();
		}//function

		public static IEnumerable<PropertyInfo> getJoins(Type type)	{	return type.GetProperties().Where(pi => pi.GetCustomAttribute<ForeignKeyAttribute>() != null);	}//function
		public static IEnumerable<PropertyInfo> getJoins<T>()		{		return getJoins(typeof(T));	}//function

	}//class
}//ns
