﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Osnova.Model
{
	public partial class Groupdype: EntityBase
	{
		[Required(AllowEmptyStrings = false)]
		[Index("IX_Groupdype_Code", IsUnique = true)]
		[MaxLength(50)]
		public string Code { get; set; }

		[Required(AllowEmptyStrings = false)]
		[Index("IX_Groupdype_Name", IsUnique = true)]
		[MaxLength(250)]
		public string Name { get; set; }

		public override string ToString() { return "{0}. Code={1}. Name={2}".Fmt(base.ToString(), Code, Name); }
	}//class
}//ns
