﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;

namespace Osnova.Model
{
	/// <summary>
	/// права на поля в зависимости от статуса
	/// </summary>
	public partial class RoleStatusContent: EntityBase
	{
		[Required]
		[Index("IX_RoleStatusContent", IsUnique = true, Order = 1)]
		public int RoleID { get; set; }
		public Role Role { get; set; }

		[Required]
		[Index("IX_RoleStatusContent", IsUnique = true, Order = 2)]
		public int StatusID { get; set; }
		public Status Status { get; set; }

		[Required]
		public string Content { get; set; }

	}//class
}//ns
