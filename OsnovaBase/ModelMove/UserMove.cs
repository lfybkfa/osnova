﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;

namespace Osnova.Model
{
	public static class UserMove
	{
		public static bool checkIsComplete(MC moveContext)
		{
			AC.Logger.Info("checkIsComplete");
			return true;
		}//function

		public static bool checkIsFalse(MC moveContext)
		{
			AC.Logger.Info("checkIsFalse");
			return false;
		}//function

		public static bool checkIsTrue(MC moveContext)
		{
			AC.Logger.Info("checkIsTrue");
			return true;
		}//function

		public static bool runAddZ(MC moveContext)
		{
			EC ec = new EC();
			ec.CopyFrom(moveContext);
			ec.Load(ec.entityID);
			(ec.entity as User).LastName += "Z";
			ec.Save();
			return true;
		}//function

		public static bool runIsFalse(MC moveContext)
		{
			AC.Logger.Info("runIsFalse");
			return false;
		}//function
	}//class
}//ns
