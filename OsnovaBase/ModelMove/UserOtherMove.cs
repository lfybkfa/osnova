﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;

namespace Osnova.Model
{
	public static class UserOtherMove
	{
		public static bool checkIsComplete(MC moveContext)
		{
			AC.Logger.Info("checkIsComplete UserOtherMove");
			return true;
		}//function

		public static bool checkIsFalse(MC moveContext)
		{
			AC.Logger.Info("checkIsFalse UserOtherMove");
			return false;
		}//function

		public static bool checkIsTrue(MC moveContext)
		{
			AC.Logger.Info("checkIsTrue UserOtherMove");
			return true;
		}//function

		public static bool runAddZ(MC moveContext)
		{
			object Bukva = moveContext.ParamGet("Bukva");
			AC.Logger.Info(Bukva, Bukva != null);
			EC ec = new EC();
			ec.CopyFrom(moveContext);
			ec.Load(ec.entityID);
			(ec.entity as User).LastName += (Bukva != null ? (string)Bukva : "Z");
			ec.Save();
			return true;
		}//function

		public static bool runIsFalse(MC moveContext)
		{
			AC.Logger.Info("runIsFalse UserOtherMove");
			return false;
		}//function
	}//class
}//ns
