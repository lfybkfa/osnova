﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;

namespace Osnova.Model
{
	public static class SpecialLink
	{
		/// <summary>
		/// все Special, принадлежащие данному Dype
		/// </summary>
		/// <param name="linkContext"></param>
		/// <returns></returns>
		public static bool OnDype(LC linkContext)
		{
			using (var context = OsnovaDB.New)
			{
				var query = context.SetNotrack<Special>().Where(o => o.DypeID == (linkContext.ParentId ?? 0));
				linkContext.list = query.ToArray();
			}//using
			return true;
		}//function
	}//class
}//ns
