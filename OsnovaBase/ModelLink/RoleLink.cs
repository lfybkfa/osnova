﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;
using Osnova.Model;

namespace Osnova.Model
{
	public static class RoleLink
	{
		[Addable][Removable]
		public static bool OnUser(LC linkContext)
		{
			using (var context = OsnovaDB.New)
			{
				var UserID = linkContext.ParentId ?? 0;
				var query = context.SetNotrack<RoleUser>()
					.Where(ru => ru.UserID == UserID).Include(ru => ru.Role).Select(ru => ru.Role);
				linkContext.list = query.ToArray();
			}//using
			return true;
		}//function

		public static bool OnUserAdd(LC linkContext, int ID)
		{
			bool result = false;
			using (var ctx = OsnovaDB.New)
			{
				var UserID = linkContext.ParentId ?? 0;
				var RoleID = ID;
				EC ecRU = new EC { type = typeof(RoleUser), UserId = linkContext.UserId };
				RoleUser ru = new RoleUser { UserID = UserID, RoleID = RoleID };
				ecRU.entity = ru;
				result = ecRU.Save();
				if (!result)
				{
					linkContext.err = ecRU.err;
					ecRU.err = null; //чтоб не мешали сборщику мусора удалить ecRU
				}//if
			}//using
			return result;
		}//function

		public static bool OnUserRemove(LC linkContext, int ID)
		{
			bool result = false;
			using (var ctx = OsnovaDB.New)
			{
				var UserID = linkContext.ParentId ?? 0;
				var RoleID = ID;
				var sql = "DELETE FROM {0} where UserID=@p0 AND RoleID=@p1".Fmt(OsnovaDB.GetTableName<RoleUser>());
				ctx.RunSQL(sql, UserID, RoleID);
				result = true;
			}//using
			return result;
		}//function
	}//class
}//ns
