﻿using Osnova.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Model
{
	public static class RoleDypeLink
	{
		public static bool OnRole(LC linkContext)
		{
			int RoleID = linkContext.ParentId ?? 0;
			using (var context = OsnovaDB.New)
			{
				var query = context.SetNotrack<RoleDype>().Where(o => o.RoleID == RoleID);
				linkContext.list = query.ToArray();
			}//using
			return true;
		}//function
	}//class
}//ns
