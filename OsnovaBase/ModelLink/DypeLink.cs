﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Osnova.Infrastructure;

namespace Osnova.Model
{
	public static class DypeLink
	{
		public static bool OnGroupDype(LC linkContext)
		{
			using (var context = OsnovaDB.New)
			{
				var query = context.SetNotrack<Dype>().Where(o => o.GroupdypeID == (linkContext.ParentId ?? 0));
				linkContext.list = query.ToArray();
			}//using
			return true;
		}//function


		public static bool UserReadAccess(LC linkContext)
		{
			int UserID = linkContext.ParentId ?? 0;
			using (var context = OsnovaDB.New)
			{
				//получили из БД список ролей
				int[] roleIds = context.SetNotrack<RoleUser>().Where(ru => ru.UserID == UserID).Select(ru => ru.RoleID).ToArray();
				//достали из памяти список дайпов по каждой роли
				linkContext.list = roleIds.SelectMany(roleId => AC.Meta.GetDypesOnRole(roleId, AccessDype.READ)).Distinct().ToArray();
			}//using
			return true;
		}//fucntion
	}//class
}//ns
