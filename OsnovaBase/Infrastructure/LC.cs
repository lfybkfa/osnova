﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Data;
using Osnova.Model;
using System.Diagnostics;
using System.ComponentModel.DataAnnotations.Schema;

namespace Osnova.Infrastructure
{
	/// <summary>
	/// Link context
	/// заполнить и запустить
	/// </summary>
	public class LC: AbstractContext
	{
		#region Properties
		public delegate bool selectDelegate(LC linkContext);
		public delegate bool addremoveDelegate(LC linkContext, int ID);
		static readonly string TypeNameFormat = "Osnova.Model.{0}Link";
		static readonly string SelectAllGeneric = "SelectAll"; //name of generic method
		private bool IsUniversal { get { return string.IsNullOrWhiteSpace(name) || name == WORD.All; } }

		public bool WithCount = false;
		public int Count = 0;

		/// <summary>
		/// тип класса, содержащего методы получения списка
		/// если не используется универсальная функция
		/// </summary>
		private Type typeLink { get { return Type.GetType(typeLinkName); } }
		private string typeLinkName { get { return type != null ? TypeNameFormat.Fmt(type.Name) : string.Empty; } }
		/// <summary>
		/// Имя линка
		/// если не указано, то будет использоваться универсальная функция
		/// </summary>
		public string name; 
		/// <summary>
		/// функция получения списка.
		/// формируется на основании typeName и name
		/// </summary>
		private selectDelegate select; 
		/// <summary>
		/// функция добавления к списку (если список позволяет это сделать).
		/// формируется на основании typeName и name
		/// </summary>
		private addremoveDelegate add;
		/// <summary>
		/// функция удаления из списка (если список позволяет это сделать).
		/// формируется на основании typeName и name
		/// </summary>
		private addremoveDelegate remove;

		/// <summary>
		/// список условий
		/// </summary>
		private ICollection<Condition> conditions;
		/// <summary>
		/// сколько записей на странице
		/// </summary>
		public int pageSize = 0;
		/// <summary>
		/// номер текущей страницы
		/// </summary>
		public int pageNum = 0;
		/// <summary>
		/// результат (null если была ошибка)
		/// </summary>
		public IEnumerable<EntityBase> list = null;
		/// <summary>
		/// список связанных сущностей, которые надо подгрузить
		/// </summary>
		private ICollection<Type> joins = null;
		/// <summary>
		/// список полей для сортировки
		/// </summary>
		private ICollection<string> orders = null;

		public IQueryable<EntityBase> query { get { return (IQueryable<EntityBase>)queryT; } }
		private object queryT = null; //для хранения IQueryable<T>
		private ParameterExpression Xentity = null; //параметр к лямдбам = typeof(T)
		#endregion

		#region функции
		/// <summary>
		/// полная обработка запроса
		/// </summary>
		/// <typeparam name="T"></typeparam>
		public void WorkAll<T>() where T : EntityBase
		{
			Xentity = Expression.Parameter(typeof(T), "entity");
			if (LoadAllJoins)	{	WorkJoins<T>();		}
			WorkConditions<T>();

			if (WithCount)
				Count = query.Count();

			WorkOrders<T>();
			WorkPaging();
		}//function

		protected bool WorkJoins<T>() where T : EntityBase
		{
			IEnumerable<PropertyInfo> properties = Dype.getJoins<T>();

			MemberExpression Xprop;
			Expression<Func<T, EntityBase>> Xlambda;

			foreach (var property in properties)
			{
				Xprop = Expression.Property(Xentity, property);
				Xlambda = Expression.Lambda<Func<T, EntityBase>>(Xprop, Xentity);
				queryT = (queryT as IQueryable<T>).Include(Xlambda);
			}//for
			return true;
		}//function

		private bool WorkConditions<T>() where T: EntityBase
		{
			if (conditions == null)		{return true;}

			Expression<Func<T, bool>> where;
			foreach (var cond in conditions)
			{
				where = cond.getExpr<T>();
				if (where != null)
					queryT = (queryT as IQueryable<T>).Where<T>(where);
				else
					err = new Err { errorCode = ErrorCode.CONDITION_NOT_PARSED, message = cond.message };
			}//for

			return true;
		}//function

		private bool WorkPaging()
		{
			if (pageNum >= 0 && pageSize > 0)
			{
				queryT = query.Skip(pageNum * pageSize).Take(pageSize);
			}//if
			
			return true;
		}//function

		private bool WorkOrders<T>() where T : EntityBase
		{
			if (orders == null) { return true; }

			bool first = true;
			bool descending;
			string propName;
			MemberExpression Xprop;
			PropertyInfo propInfo;
			Expression<Func<T, object>> Xlambda;
			Expression<Func<T, int>> XlambdaInt;
			Expression<Func<T, DateTime>> XlambdaDate;
			foreach (string s in orders)
			{
				if (s.EndsWith(WORD.DESC)) //"asd DESC"
				{
					propName = s.Substring(0, s.Length - WORD.DESC.Length - 1);
					descending = true;
				}//if
				else
				{
					propName = s;
					descending = false;
				}//else
				
				propInfo = type.GetProperty(propName);
				if (propInfo == null)
					continue;

				Xlambda = null; XlambdaDate = null; XlambdaInt = null;
				Xprop = Expression.Property(Xentity, propInfo);
				if (propInfo.PropertyType == typeof(int))
					XlambdaInt = Expression.Lambda<Func<T, int>>(Xprop, Xentity);
				else if (propInfo.PropertyType == typeof(DateTime))
					XlambdaDate = Expression.Lambda<Func<T, DateTime>>(Xprop, Xentity);
				else
					Xlambda = Expression.Lambda<Func<T, object>>(Xprop, Xentity);

				if (first)
				{
					first = false;
					if (descending)
						queryT = XlambdaInt != null ?
							(queryT as IQueryable<T>).OrderByDescending(XlambdaInt)
							: XlambdaDate != null ?
							(queryT as IQueryable<T>).OrderByDescending(XlambdaDate)
							: (queryT as IQueryable<T>).OrderByDescending(Xlambda);
					else
						queryT = XlambdaInt != null ?
						(queryT as IQueryable<T>).OrderBy(XlambdaInt)
						: XlambdaDate != null ?
						(queryT as IQueryable<T>).OrderBy(XlambdaDate)
						: (queryT as IQueryable<T>).OrderBy(Xlambda);

				}//if
				else
				{
					if (descending)
						queryT = XlambdaInt != null ?
							(queryT as IOrderedQueryable<T>).ThenByDescending(XlambdaInt)
							: XlambdaDate != null ?
							(queryT as IOrderedQueryable<T>).ThenByDescending(XlambdaDate)
							: (queryT as IOrderedQueryable<T>).ThenByDescending(Xlambda);
					else
						queryT = XlambdaInt != null ?
						(queryT as IOrderedQueryable<T>).ThenBy(XlambdaInt)
						: XlambdaDate != null ?
						(queryT as IOrderedQueryable<T>).ThenBy(XlambdaDate)
						: (queryT as IOrderedQueryable<T>).ThenBy(Xlambda);

				}//else
			}//for
			return true;
		}//function

		private bool CheckServiceType()
		{
			if (typeLink == null)
			{
				err = new Err { errorCode = ErrorCode.LINK_TYPE_NOT_FOUND, message = "Type={1}".Fmt(typeLinkName) };
				return false;
			}//if
			return true;
		}//function


		public bool Select()
		{
			if (CheckUserIsSet() == false)
				return false;

			if (CheckUserCanRead() == false)
				return false;

			try
			{
				if (IsUniversal)
				{
					Type t = typeof(LC);
					MethodInfo mi = t.GetMethod(SelectAllGeneric);
					MethodInfo miGeneric = mi.MakeGenericMethod(type);
					bool result = (bool)miGeneric.Invoke(this, null);
					return result;
				}//if
				else
				{
					if (CheckServiceType() == false)
						return false;

					if (select == null)
					{
						var methodName = name; //может быть name + "Select"?
						var methodInfo = typeLink.GetMethod(methodName);
						if (methodInfo == null)
						{
							err = new Err { errorCode = ErrorCode.METHOD_LINK_NOT_FOUND, message = "Method={0}".Fmt(methodName) };
							return false;
						}//if
						select = (selectDelegate)methodInfo.CreateDelegate(typeof(selectDelegate));
					}//if

					//получаем результат
					if (select(this) == false)
					{
						err = new Err { errorCode = ErrorCode.LINK_NOT_LOADED };
						return false;
					}//if
				}//else
			}//try
			catch (Exception exception)
			{
				err = new Err { errorCode = ErrorCode.LINK_NOT_LOADED, exception = exception };
				return false;
			}//catch
			return true;
		}//function

		public bool SelectAll<T>() where T: EntityBase
		{
			type = typeof(T);
			using (var context = OsnovaDB.New)
			{
				queryT = context.SetNotrack<T>();
				WorkAll<T>();

				list = query.ToArray();
			}//using
			return true;

		}//function

		public bool Add(int ID)
		{
			if (CheckUserIsSet() == false)
				return false;

			if (CheckUserCanRead() == false)
				return false;

			if (CheckServiceType() == false)
				return false;

			try
			{
				if (add == null)
				{
					var methodName = name + WORD.Add;
					var methodInfo = typeLink.GetMethod(methodName);
					if (methodInfo == null)
					{
						err = new Err { errorCode = ErrorCode.METHOD_LINK_NOT_FOUND, message = "Method={0}".Fmt(methodName) };
						return false;
					}//if
					add = (addremoveDelegate)methodInfo.CreateDelegate(typeof(addremoveDelegate));
				}//if

				//получаем результат
				if (add(this, ID) == false)
				{
					err = err ?? new Err { errorCode = ErrorCode.LINK_NOT_ADDED };
					return false;
				}//if
			}//try
			catch (Exception exception)
			{
				err = new Err { errorCode = ErrorCode.LINK_NOT_ADDED, exception = exception };
				return false;
			}//catch
			return true;
		}//function

		public bool Remove(int ID)
		{
			if (CheckUserIsSet() == false)
				return false;

			if (CheckUserCanRead() == false)
				return false;

			if (CheckServiceType() == false)
				return false;

			try
			{
				if (remove == null)
				{
					var methodName = name + WORD.Remove;
					var methodInfo = typeLink.GetMethod(methodName);
					if (methodInfo == null)
					{
						err = new Err { errorCode = ErrorCode.METHOD_LINK_NOT_FOUND, message = "Method={0}".Fmt(methodName) };
						return false;
					}//if
					remove = (addremoveDelegate)methodInfo.CreateDelegate(typeof(addremoveDelegate));
				}//if

				//получаем результат
				if (remove(this, ID) == false)
				{
					err = err ?? new Err { errorCode = ErrorCode.LINK_NOT_REMOVED };
					return false;
				}//if
			}//try
			catch (Exception exception)
			{
				err = new Err { errorCode = ErrorCode.LINK_NOT_REMOVED, exception = exception };
				return false;
			}//catch
			return true;
		}//function

		private MethodInfo methodInfo
		{
			get
			{
				if (select != null)
					return select.GetMethodInfo();

				if (typeLink == null)
					return null;

				return typeLink.GetMethod(name);
			}//get
		}//prop

		public void AddOrder(string order)
		{
			if (orders == null) { orders = new List<string>(); }//if
			orders.Add(order);
		}//function

		public void AddCondition(Condition c)
		{
			if (conditions == null)			{				conditions = new List<Condition>();			}//if
			conditions.Add(c);
		}//function

		public void AddJoin<T>() where T:EntityBase { AddJoin(typeof(T));}

		public void AddJoin(Type t)
		{
			//нельзя джойнить типы какие-попало, только наследников от EntityBase
			if (t.IsSubclassOf(typeof(EntityBase)) == false)
				return;

			//инициализироваем, если надо
			if (joins == null)			{				joins = new List<Type>();			}//if
			joins.Add(t);
		}//function

		public DataSet CreateDataSet()
		{
			if (list == null)			{				return null;			}//if

			string MainTableName = WORD.Main;
			DataSet ds = new DataSet();
			DataTable dt;
			IEnumerable<string> fieldsRead = AC.Meta.GetUserFields(AC.Room.Get(UserId), typeName, AccessDype.READ).ToArray();
			if (fieldsRead == null) //нет доступа
			{
				return ds;
			}//if
			IEnumerable<Field> fields = AC.Meta.GetDype(typeName).FieldsAll.Where(f => fieldsRead.Contains(f.Name)).ToArray();

			#region actions
			Action<DataTable, IEnumerable<Field>> createTable = (table, flds) => 
			{
				foreach (Field f in flds)
				{
					table.Columns.Add(f.CreateColumn());
				}//for
			};

			Action<DataTable, IEnumerable<Field>, IEnumerable<EntityBase>> fillTable = (table, flds, entities) =>
			{
				DataRow dr;
				foreach (EntityBase entity in entities)
				{
					dr = table.NewRow();
					foreach (Field f in flds) { dr[f.Name] = f.property.GetValue(entity) ?? DBNull.Value; }//for
					table.Rows.Add(dr);
				}//for
			};
			#endregion
			
			#region create and fill main table
			dt = new DataTable(MainTableName);
			createTable(dt, fields);
			fillTable(dt, fields, list);
			ds.Tables.Add(dt);
			#endregion

			#region create and fill join tables
			if (joins != null)
			{
				Dype dype;
				IEnumerable<EntityBase> entJoin;
				ComparerOfEntityBaseOnID comparer = new ComparerOfEntityBaseOnID();
				foreach (Type typeJoin in joins)
				{
					dype = AC.Meta.GetDype(typeJoin);
					if (dype == null)
						continue;

					entJoin = list.Select(ent => ent.NavigationValue(dype.Code))
						.Where(ent => ent != null)
						.Distinct(comparer);
					dt = new DataTable(dype.Code);
					fields = dype.FieldsAll;
					createTable(dt, fields);
					fillTable(dt, fields, entJoin);
					ds.Tables.Add(dt);
				}//for
			}//if
			#endregion

			#region add service tables
			var realUserID = list.Select(ent => ent.UserCreate)
				.Union(list.Select(ent => ent.UserChange))
				.Union(list.Select(ent => ent.UserResponsible ?? 0))
				.Distinct().ToArray();
			var realRows = AC.Meta.dtUsers.AsEnumerable()
				.Where(dr => realUserID.Contains(dr.Field<int>(WORD.ID)));

			DataTable dtUsers = AC.Meta.dtUsers.Clone();
			dtUsers.BeginLoadData();
			realRows.ForEach(dr => dtUsers.Rows.Add(dr.ItemArray));
			dtUsers.EndLoadData();
			dtUsers.AcceptChanges();
			ds.Tables.Add(dtUsers);
			
			#endregion
			return ds;
		}//function
		#endregion
	}//class
}//ns
