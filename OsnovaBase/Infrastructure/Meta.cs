﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Model;
using System.Data;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Source")]
namespace Osnova.Infrastructure
{
	internal class Meta
	{
		#region nested class FieldPackage
		struct FieldPackage
		{
			public int DypeID;
			public int RoleID;
			public int StatusID;
			string[] FieldsRead;
			string[] FieldsWrite;

			public bool HasContent { get { return FieldsRead != null || FieldsWrite != null; } }
			public IEnumerable<string> Fields(AccessDype access)
			{
				return (access == AccessDype.READ) ? FieldsRead : FieldsWrite;
			}//function

			/// <summary>
			/// заполнение из строчного массива
			/// </summary>
			/// <param name="Content"></param>
			private void FillFromContent(string Content)
			{
				if (Content.IsNothing()) { return; }

				//будем работать с полным списком полей
				//шанс, что dype с таким Ид отсутвует - игнорируем, потому что это проверяется целостностью БД
				Dype dype = AC.Meta.GetDype(DypeID);
				IEnumerable<string> FieldsAll = dype.FieldsAll.Select(fld => fld.Name).ToArray();

				//сплитуем в построчный массив
				string Key;
				string[] Values;
				foreach (var s in Content.Lines())
				{
					//Read=field1 field2  
					Key = s.IniKey();
					//берем часть "field1 field2", сплитуем в массив, заносим массив в список полей
					Values = s.IniValue().Split(CHAR.DELIM);

					//каждый список (FieldsRead, FieldsWrite) будет заполняться только один раз
					//при этoм приоритет у Not
					if (false) { }
					else if (FieldsRead == null && Key == WORD.ReadNot)
					{
						FieldsRead = FieldsAll.Except(Values).ToArray();
					}//if
					else if (FieldsRead == null && Key == WORD.Read)
					{
						FieldsRead = Values.Where(val => FieldsAll.Contains(val)).ToArray();
					}//if
					else if (FieldsWrite == null && Key == WORD.WriteNot)
					{
						FieldsWrite = FieldsAll.Except(Values).ToArray();
					}//if
					else if (FieldsWrite == null && Key == WORD.Write)
					{
						FieldsWrite = Values.Where(val => FieldsAll.Contains(val)).ToArray();
					}//if
				}//for
			}//function

			public static FieldPackage CreateFrom(RoleDype rd)
			{
				FieldPackage result = new FieldPackage();
				result.DypeID = rd.DypeID;
				result.RoleID = rd.RoleID;
				result.StatusID = 0;
				result.FillFromContent(rd.Content);
				return result;
			}//function

			public static FieldPackage CreateFrom(RoleStatusContent rd)
			{
				FieldPackage result = new FieldPackage();
				result.DypeID = AC.Meta.GetStatus(rd.StatusID).DypeID;
				result.RoleID = rd.RoleID;
				result.StatusID = rd.StatusID;
				result.FillFromContent(rd.Content);
				return result;
			}//function

			public override string ToString()
			{
				if (StatusID == 0)
				{
					return "Dype=".Add(AC.Meta.GetDype(DypeID).Code, STR.ZPT
						, FieldsRead != null ? WORD.Read : String.Empty
						, FieldsWrite != null ? WORD.Write : String.Empty
						);
				}//if
				else
					return String.Empty.Add(
							"Dype=", AC.Meta.GetDype(DypeID).Code, STR.ZPT
						, "Status=", AC.Meta.GetStatus(StatusID).Code, STR.ZPT
						, FieldsRead != null ? WORD.Read : String.Empty
						, FieldsWrite != null ? WORD.Write : String.Empty
						);
			}//function
		}//class
		#endregion

		ICollection<Dype> DypeSet = new HashSet<Dype>();
		ICollection<Role> RoleSet = new HashSet<Role>();
		ICollection<Report> ReportSet = new HashSet<Report>();
		ICollection<Special> SpecialSet = new HashSet<Special>();
		ICollection<Status> StatusSet = new HashSet<Status>();

		ICollection<RoleDype> RoleDypeSet = new HashSet<RoleDype>();
		ICollection<RoleReport> RoleReportSet = new HashSet<RoleReport>();
		ICollection<RoleSpecial> RoleSpecialSet = new HashSet<RoleSpecial>();
		ICollection<RoleStatus> RoleStatusSet = new HashSet<RoleStatus>();
		ICollection<RoleStatusContent> RoleStatusContentSet = new HashSet<RoleStatusContent>();

		#region fields
		/// <summary>
		/// набор непустых FieldPackage
		/// </summary>
		IEnumerable<FieldPackage> fieldPackageS;
		private Field[] baseFields;
		public static readonly string[] serviceFields = { "UserCreate", "UserChange", "CreateDate", "ChangeDate", "Stat", "UserFix" };
		public IEnumerable<Field> BaseFields { get { return baseFields; } }
		#endregion

		public DataTable dtUsers = new DataTable("Users");

		public long Size = 0;

		private bool RefreshSet<T>(ICollection<T> set, LC lc) where T : EntityBase
		{
			bool result = false;
			result = lc.SelectAll<T>();
			if (result)
			{
				set.Clear();
				lc.list.Cast<T>().ForEach(o => set.Add(o));
			}//if
			else
			{
				AC.Logger.Error(lc.err.fullInfo);
			}//else
			return result;
		}//function

		public void Refresh()
		{
			long BeforeBytes = System.GC.GetTotalMemory(true);

			LC lc = new LC();

			#region грузим из БД метаданные
			RefreshSet<Dype>(DypeSet, lc);
			RefreshSet<Role>(RoleSet, lc);
			RefreshSet<Report>(ReportSet, lc);
			RefreshSet<Special>(SpecialSet, lc);
			RefreshSet<Status>(StatusSet, lc);

			RefreshSet<RoleDype>(RoleDypeSet, lc);
			RefreshSet<RoleReport>(RoleReportSet, lc);
			RefreshSet<RoleSpecial>(RoleSpecialSet, lc);
			RefreshSet<RoleStatus>(RoleStatusSet, lc);
			RefreshSet<RoleStatusContent>(RoleStatusContentSet, lc);
			#endregion

			#region грузим юзверей из БД
			dtUsers.Clear();
			dtUsers.Columns.Clear();
			dtUsers.Columns.Add(WORD.ID, typeof(int));
			dtUsers.Columns.Add(WORD.Name, typeof(string));
			lc.SelectAll<User>();
			lc.list.Cast<User>().ForEach(u => dtUsers.Rows.Add(u.ID, u.FIO));
			dtUsers.Rows.Add(0, "=Начальная загрузка=");
			#endregion

			#region Обрабатываем
			//заполняем список полей, общий для всех
			baseFields = Field.Get(typeof(EntityBase)).Where(f => f.Name != "UserFix").ToArray();
			//заполняем список полей для каждого Dype
			DypeSet.ForEach(d => d.Prepare());
			//заполняем список полей, общий для всех
			baseFields = typeof(EntityBase).GetFieldsDype().Where(f => f.Name != "UserFix").ToArray();

			//ставим признак наличия статусов для каждого Dype
			StatusSet.Select(st => st.DypeID).Distinct()
				.ForEach(dypeId => DypeSet.First(d => d.ID == dypeId).HasStatus = true);

			//статусы
			StatusSet.ForEach(s => s.Prepare());

			//формируем списки полей для ролей и статусов
			//берем только заполненные
			IEnumerable<FieldPackage> fpS = RoleDypeSet.Select(rd => FieldPackage.CreateFrom(rd)).Where(fp => fp.HasContent);
			IEnumerable<FieldPackage> fpSC = RoleStatusContentSet.Select(rd => FieldPackage.CreateFrom(rd)).Where(fp => fp.HasContent);
			fieldPackageS = fpS.Union(fpSC).ToArray();
			#endregion

			long AfterBytes = System.GC.GetTotalMemory(true);
			Size = AfterBytes - BeforeBytes;
		}//function

		public bool CheckUserCanRun(UC userContext, string dypeCode, string specialCode)
		{
			Special sp = GetSpecial(dypeCode, specialCode);
			if (sp == null)
				return false;

			bool result = false;
			IEnumerable<RoleSpecial> list = RoleSpecialSet
				.Where(rs => userContext.roles.Contains(rs.RoleID) && rs.SpecialID == sp.ID);
			result = list.Any();
			return result;
		}//function

		public bool CheckUserCanMove(UC userContext, string dypeCode, string statusCode)
		{
			Status st = GetStatus(dypeCode, statusCode);
			if (st == null)
				return false;

			bool result = false;
			IEnumerable<RoleStatus> list = RoleStatusSet
				.Where(rs => userContext.roles.Contains(rs.RoleID) && rs.StatusID == st.ID);
			result = list.Any();
			return result;
		}//function

		public bool CheckUserHasFlag(UC userContext, string dypeCode, AccessDype flag)
		{
			Dype dype = GetDype(dypeCode);
			if (dype == null)
				return false;

			bool result = false;
			IEnumerable<RoleDype> drs = RoleDypeSet.Where(dr => userContext.roles.Contains(dr.RoleID) && dr.DypeID == dype.ID);
			result = drs.Any(dr => dr.Access.HasFlag(flag));
			return result;
		}//function

		public Special GetSpecial(string dypeCode, string specialCode)
		{
			Dype dype = GetDype(dypeCode);
			if (dype == null)
				return null;

			Special result = SpecialSet.FirstOrDefault(sp => sp.Code == specialCode && sp.DypeID == dype.ID);
			return result;
		}//function

		public Dype GetDype(int DypeID) { return DypeSet.FirstOrDefault(d => d.ID == DypeID); }//function
		public Dype GetDype(string dypeCode)	{	return DypeSet.FirstOrDefault(d => d.Code == dypeCode);	}//function
		public Dype GetDype(Type type) { return GetDype(type.Name); }//function
		public Role GetRole(int RoleID)		{	return RoleSet.FirstOrDefault(r => r.ID == RoleID);		}//function
		public Role GetRole(string RoleCode) { return RoleSet.FirstOrDefault(r => r.Code == RoleCode); }//function

		public IEnumerable<Dype> GetDypesOnRole(int RoleId, AccessDype access)
		{
			return RoleDypeSet.Where(rd => rd.Access.HasFlag(access) && rd.RoleID == RoleId)
				.Select(rd => DypeSet.FirstOrDefault(d => d.ID == rd.DypeID));
		}//function

		public IEnumerable<RoleDype> GetRoleDypesOnRole(int RoleId, AccessDype access)
		{
			return RoleDypeSet.Where(rd => rd.Access.HasFlag(access) && rd.RoleID == RoleId);
		}//function

		#region fields



		/// <summary>
		/// спиок полей, доступных пользователю для данного типа с учетом статуса
		/// </summary>
		/// <param name="userContext">пользователь</param>
		/// <param name="dypeCode">код типа</param>
		/// <param name="statusCode">код статуса (может быть null)</param>
		/// <param name="access">чтение-запись</param>
		/// <returns>null ошибка в параметрах</returns>
		public IEnumerable<string> GetUserFields(UC userContext, string dypeCode, AccessDype access, string statusCode = null)
		{
			Dype dype = GetDype(dypeCode);
			if (dype == null)	{return null;}

			//получить список доступов, где есть ограничения на поля
			IEnumerable<FieldPackage> fpS;
			if (statusCode.IsNothing())//без учета статуса
			{
				fpS = userContext.roles.SelectMany(roleID =>
					fieldPackageS.Where(fp => fp.RoleID == roleID && fp.DypeID == dype.ID && fp.StatusID == 0))
					.ToArray();

				//если ограничений нет вообще или есть хоть одно пустое
				if (fpS.Any() == false || fpS.Any(fp => fp.Fields(access) == null))
				{
					//значит доступны все поля
					return dype.FieldsAll.Select(f => f.Name);
				}//if
			}//if
			else //с учетом статуса
			{
				Status status = GetStatus(dypeCode, statusCode);
				if (status == null) { return null; }//если уж указан, то статус должен быть правильным
				fpS = userContext.roles.SelectMany(roleID =>
					 fieldPackageS.Where(fp => fp.RoleID == roleID && fp.DypeID == dype.ID && fp.StatusID == status.ID))
					 .ToArray();

				//если ограничений нет вообще или есть хоть одно пустое
				if (fpS.Any() == false || fpS.Any(fp => fp.Fields(access) == null))
				{
					//тогда смотрим ограничения без учета статуса
					return GetUserFields(userContext, dypeCode, access);
				}//if
			}//else

			//теперь у нас только допуски с ограничением по полям
			//объединяем списки полей, т.е. если поле доступно хотя бы в одном списке, то оно доступно вообще
			HashSet<string> result = new HashSet<string>();
			foreach (var fp in fpS)
			{
				result.UnionWith(fp.Fields(access));
			}//for

			return result;
		}//function
		#endregion

		#region status
		internal Status GetStatus(int statusID) { return StatusSet.FirstOrDefault(s => s.ID == statusID); }
		public Status GetStatus(string dypeCode, string statusCode) 
		{
			Dype dype = GetDype(dypeCode);
			Status result = StatusSet.FirstOrDefault(s => s.DypeID == dype.ID && s.Code == statusCode);
			return result;
		}//function

		public Status GetStatus<T>(string statusCode)
		{
			Dype dype = GetDype(typeof(T));
			Status result = StatusSet.FirstOrDefault(s => s.DypeID == dype.ID && s.Code == statusCode);
			return result;
		}//function
		#endregion

		#region reports
		internal Report GetReport(string ReportCode)
		{
			return ReportSet.FirstOrDefault(r => r.Code == ReportCode);
		}//function

		internal Report GetReport(int ReportID)
		{
			return ReportSet.FirstOrDefault(r => r.ID == ReportID);
		}//function

		internal bool CheckUserCanReport(UC userContext, int ReportID)
		{
			Report rpt = GetReport(ReportID);
			if (rpt == null) { return false; }
			return RoleReportSet.Any(rr => userContext.roles.Contains(rr.RoleID) && rr.ReportID == rpt.ID);
		}//function

		internal IEnumerable<Report> GetReportsForRole(int RoleID)
		{
			return RoleReportSet.Where(rr=> rr.RoleID == RoleID)
				.Select(rr => GetReport(rr.ReportID))
				.Where(r => r != null);
		}//function

		internal IEnumerable<Report> GetReportsForUser(UC userContext)
		{
			return userContext.roles.SelectMany(RoleID => GetReportsForRole(RoleID)).Distinct();
		}//function
		#endregion
	}//class
}//ns
