﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Model;
using System.Data.Entity.Infrastructure;
using System.Reflection;

namespace Osnova.Infrastructure
{
	/// <summary>
	/// Entity Context
	/// контекст работы с entity
	/// </summary>
	public class EC : AbstractContext
	{
		public bool Save()
		{
			if (CheckUserIsSet() == false) { return false; }
			if (CheckUserCanWrite() == false) { return false; }
			if (CheckEntityIsSet() == false) { return false; }
			if (CheckEntityIsFixed() == false) { return false; }
			if (CheckEntityIsLocked() == false) { return false; }
			if (CheckStatusIsWritable() == false) { return false; }

			IEnumerable<Err> validate_erors = entity.Validate();
			if (validate_erors != null)
			{
				validate_erors.ForEach(validate_err => err = validate_err);
				err = new Err { errorCode = ErrorCode.ENTITY_NOT_VALIDATED };
				return false;
			}//if

			//now save
			DbContext ctx = null;
			try
			{
				entity.SetUserAndDate(UserId);
				entity.BeforeSave(this);

				//если задан внешний контекст, берем его, иначе создем новый
				ctx = HasOuterContext ? outerCtx : OsnovaDB.New;
				ctx.Set(entity.GetType()).Add(entity);
				ctx.Entry(entity).State = (entity.ID != 0) ? EntityState.Modified : EntityState.Added;
				ctx.SaveChanges();
				entity.AfterSave(this);

				ClearLockIfExists();
			}//try
			catch (Exception exception)
			{
				err = new Err { errorCode = ErrorCode.ENTITY_NOT_SAVED, exception = exception, message = entity.ToString() };
				return false;
			}//catch
			finally
			{
				//если свой контекст, то грохаем
				if (HasOuterContext == false && ctx != null)
				{
					ctx.Dispose();
				}//if
			}//fin

			return true;
		}//function

		public bool Load(int ID)
		{
			if (CheckUserIsSet() == false)
				return false;

			if (CheckUserCanRead() == false)
				return false;

			if (type == null)
			{
				err = new Err { errorCode = ErrorCode.TYPE_NOT_ENTITY };
				return false;
			}//if

			try
			{
				using (var context = OsnovaDB.New)
				{
					//entity = (EntityBase)context.Set(type).Find(ID);
					if (LoadAllJoins)
					{
						MethodInfo method = typeof(OsnovaDB).GetMethod("GetOnIDWithJoins");
						MethodInfo generic = method.MakeGenericMethod(type);
						var obj = generic.Invoke(context, new object[] {ID});
						entity = (EntityBase)obj;
					}//if
					else
					{
						entity = (EntityBase)context.Set(type).Find(ID);
					}//else

					if (entity == null)
					{
						err = new Err { errorCode = ErrorCode.ENTITY_NOT_FOUND };
						return false;
					}//if
				}//using
				entity.AfterLoad(this);

				//если надо лочить, а уже залочено, то ошибка.
				//но документ все-таки загружен, им можно пользоваться
				if (CheckEntityIsLocked() == false)				{ return false; }

				//надо лочить? лочим
				SaveLockIfNeeded();
			}//try
			catch (Exception exception)
			{
				err = new Err { errorCode = ErrorCode.ENTITY_NOT_LOADED, exception = exception };
				return false;
			}//catch
		
			return true;
		}//function

		public bool Delete()
		{
			if (CheckUserIsSet() == false) { return false; }
			if (CheckUserCanWrite() == false) { return false; }
			if (CheckEntityIsSet() == false) { return false; }
			if (CheckEntityIsFixed() == false) { return false; }
			if (CheckEntityIsLocked() == false) { return false; }
			if (CheckStatusIsWritable() == false) { return false; }

			bool result = true;
			DbContext ctx = null;
			try
			{
				entity.BeforeDelete(this);
				ctx = HasOuterContext ? outerCtx : OsnovaDB.New;
				ctx.Set(entity.Type).Add(entity);
				ctx.Entry(entity).State = EntityState.Deleted;
				int rows = ctx.SaveChanges();
				if (rows != 1)
				{
					err = new Err { errorCode = ErrorCode.ENTITY_NOT_FOUND };
					result = false;
				}//if
				else
					entity.AfterDelete(this);
			}//try
			catch (Exception exception)
			{
				err = new Err { errorCode = ErrorCode.ENTITY_NOT_DELETED, exception = exception, message = entity.ToString() };
				result = false;
			}//catch
			finally
			{
				//если свой контекст, то грохаем
				if (HasOuterContext == false && ctx != null)	{		ctx.Dispose();	}//if
			}//fin
			return result;
		}//function

		private bool CheckEntityIsFixed()
		{
			if (entity.UserFix != null)
			{
				err = new Err { errorCode = ErrorCode.ENTITY_IS_FIXED, message = "Fixed by {0}".Fmt(entity.UserFix.ToString()) };
				return false;
			}//if
			else
				return true;
		}//function

		public bool Fix(bool Yes)
		{
			if (CheckUserIsSet() == false) { return false; }
			if (CheckUserCanFix() == false) { return false; }
			if (CheckEntityIsSet() == false) { return false; }
			//if (CheckEntityIsFixed() == false) { return false; }
			if (CheckEntityIsLocked() == false) { return false; }

			bool result = true;
			try
			{
				string fixValue = (Yes) ? UserId.ToString() : WORD.NULL;
				string sql = "UPDATE {0} SET UserFix = {1} WHERE ID = {2}".Fmt(entity.Table, fixValue, entity.ID);
				using (var ctx = OsnovaDB.New)
				{
					result = (ctx.RunSQL(sql) > 0);
				}//using
			}//try
			catch (Exception exception)
			{
				err = new Err { errorCode = ErrorCode.ENTITY_FIX_ERROR, exception = exception, message = entity.ToString() };
				result = false;
			}//catch
			return result;
		}//function


		/// <summary>
		/// забрать entity и стереть ее из контекста
		/// нужно для дальнейшей работы с entity и освобождения контекста для сборщика мусора
		/// </summary>
		/// <returns></returns>
		public EntityBase TakeEntityAndClear()
		{
			EntityBase result = entity;
			entity = null;
			return result;
		}//function
	}//class
}//ns
