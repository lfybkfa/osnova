﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Infrastructure
{
	/// <summary>
	/// cache
	/// TODO: переделать на многопоточность
	/// </summary>
	public class Cache: ICache
	{
		ICollection<EntityBase> data;
		int MaxSize;
		int ClearMax = 100;
		int ClearCount = 0;
		bool TimeToCheckNeedForClear { get { return ClearCount < ClearMax; } }

		public Cache(int InitialSize, int MaxSize)
		{
			data = new List<EntityBase>(InitialSize);
			this.MaxSize = MaxSize;
		}//constructor

		public void Add(EntityBase entity)
		{
			data.Add(entity);
		}//function

		public EntityBase Get(Type type, int ID)
		{
			return data.FirstOrDefault(e => (e.ID == ID && e.GetType() == type) );
		}//function
	}//class
}//ns
