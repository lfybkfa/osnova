﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Model;
using System.Reflection;
using System.Linq.Expressions;

namespace Osnova.Infrastructure
{
	abstract public class AbstractContext
	{
		public Type type;
		public string typeName 
		{ 
			get { return type == null ? string.Empty : type.Name; } 
			set { type = EntityBase.GetType(value); } 
		}

//		private Dype _dype;
		//protected Dype dype { get { if (_dype == null) { _dype = AC.Meta.GetDype(type); } return _dype; } }
		protected Dype dype { get {return AC.Meta.GetDype(type); } }
		/// <summary>
		/// заполнение данного контекста из переданного контекста
		/// Копируются: тип ИД юзер внешнееСоединение
		/// </summary>
		/// <param name="src">откуда берутся данные</param>
		/// <returns></returns>
		public virtual AbstractContext CopyFrom(AbstractContext src)
		{
			type = src.type;
			entityID = src.entityID;
			UserId = src.UserId;
			outerCtx = src.outerCtx;
			return this;
		}//function

		/// <summary>
		/// количество записей полученных последним запросом
		/// </summary>
		public int rows { get; protected set; }

		public int? ParentId { get; set; }

		#region params work
		private IDictionary<string, object> Params = null;
		public void ParamSet(string key, object value)
		{
			if (Params == null) { Params = new Dictionary<string, object>(); }
			Params[key] = value;
		}//function

		public object ParamGet(string key)
		{
			if (Params == null) { return null; }
			return Params[key];
		}//function

		/// <summary>
		/// список имен параметров
		/// никогда не возвращает NULL (если параметров нет, то возвращает пустой массив строк)
		/// </summary>
		public IEnumerable<string> ParamNames
		{
			get { return Params == null ? new string[] { } : Params.Keys; }
		}//function
		#endregion

		#region outer connection
		protected OsnovaDB outerCtx = null;
		protected bool HasOuterContext { get { return outerCtx != null; } }
		protected DbContextTransaction tran { get; set; }
		#endregion

		#region entity work
		public int entityID;
		protected EntityBase _entity;
		public string entityInfo { get { return (_entity != null) ? _entity.ToString() : "entity is null"; } }
		public EntityBase entity 
		{ 
			get { return _entity; } 
			set { _entity = value; type = (value != null) ? value.Type : null; } 
		}//prop

		protected bool CheckEntityIsSet()
		{
			if (entity == null)
			{
				err = new Err { errorCode = ErrorCode.ENTITY_IS_NULL };
				return false;
			}//if
			return true;
		}//function
		#endregion

		#region Lock (Idea=d120315)
		public bool WithLock = false;	
		public bool ClearLock = false;
		private Lock myLock = null;

		/// <summary>
		/// создать захват на основе контекста
		/// </summary>
		/// <returns></returns>
		protected Lock CreateLock()
		{
			if (UserId <= 0) { return null; }
			if (entity == null) { return null; }
			if (entity.ID <= 0) { return null; }


			Dype dype = AC.Meta.GetDype(type);
			if (dype == null) { return null; }

			Lock result = new Lock() { DypeId = dype.ID, EntityId = entity.ID, UserId = UserId };
			return result;
		}//function

		protected void ClearLockIfExists()	{	if (ClearLock) 	{		myLock.Delete();	}	}//function
		protected void SaveLockIfNeeded()		{	if (WithLock)	 { myLock.Save(); }		}//function

		protected bool CheckEntityIsLocked()
		{
			if (WithLock)
			{
				myLock = myLock ?? CreateLock();
				Lock dbLock = Lock.Find(myLock);
				if (dbLock != null)
				{
					if (dbLock.UserId == UserId) // свой захват освежаем, типа мы еще здесь работаем
					{
						myLock.Refresh();
					}//if
					else //не мы захват сделали, обижаемся и уходим
					{
						err = new Err { errorCode = ErrorCode.ENTITY_IS_LOCK, message = dbLock.ToString() };
						return false;
					}//else
				}//if
			}//if
			return true;
		}//function
		#endregion

		#region error work
		/// <summary>
		/// информация об ошибке (null если все ОК)
		/// </summary>
		protected ICollection<Err> errors = null;
		/// <summary>
		/// последняя ошибка
		/// </summary>
		public Err err 
		{ 
			get { return errors != null ? errors.LastOrDefault() : null; } 
			set { if (errors == null) { errors = new HashSet<Err>(); } errors.Add(value); } 
		}
		/// <summary>
		/// текстовое описание последней ошибки
		/// </summary>
		public string errInfo { get { return (err != null) ? err.fullInfo : "there is NO err"; } }
		/// <summary>
		/// количество ошибок
		/// </summary>
		public int errCount { get { return errors.Count(); } }
		/// <summary>
		/// получение ошибки под конкретным номером
		/// </summary>
		/// <param name="index"></param>
		/// <returns>null, если нет с таким номером</returns>
		public Err errGet(int index) { return errors.ElementAtOrDefault(index); }//function
		/// <summary>
		/// возвращает список ошибок с данным кодом
		/// </summary>
		/// <param name="code"></param>
		/// <returns></returns>
		public IEnumerable<Err> errGet(ErrorCode code) { return errors.Where(e => e.errorCode == code); }//function
		#endregion

		#region user work
		/// <summary>
		/// идентификатор пользователя, делающего запрос
		/// </summary>
		public int UserId { get { return userContext == null ? 0 : userContext.UserId; } set { userContext = AC.Room.Get(value); } }
		protected UC userContext;

		protected bool CheckUserIsSet()
		{
			if (userContext == null)
			{
				err = new Err { errorCode = ErrorCode.USER_NOT_LOGINED };
				return false;
			}//if

			return true;
		}//function

		protected bool CheckUserCanCreate()
		{
			if (AC.Meta.CheckUserHasFlag(userContext, type.Name, AccessDype.ADD) == false)
			{
				err = new Err { errorCode = ErrorCode.USER_NOT_WRITE_ACCESS, message = type.Name };
				return false;
			}//if
			return true;
		}//function

		protected bool CheckUserCanRead()
		{
			if (AC.Meta.CheckUserHasFlag(userContext, type.Name, AccessDype.READ) == false)
			{
				err = new Err { errorCode = ErrorCode.USER_NOT_READ_ACCESS, message = type.Name };
				return false;
			}//if
			return true;
		}//function

		protected bool CheckUserCanWrite()
		{
			if (AC.Meta.CheckUserHasFlag(userContext, type.Name, AccessDype.WRITE) == false)
			{
				err = new Err { errorCode = ErrorCode.USER_NOT_WRITE_ACCESS, message = type.Name };
				return false;
			}//if
			return true;
		}//function

		protected bool CheckUserCanFix()
		{
			if (AC.Meta.CheckUserHasFlag(userContext, type.Name, AccessDype.FIX) == false)
			{
				err = new Err { errorCode = ErrorCode.USER_NOT_FIX_ACCESS, message = type.Name };
				return false;
			}//if
			return true;
		}//function
		#endregion

		#region status work
		protected bool CheckStatusIsWritable()
		{
			if (entity != null && entity.Stat != null && AC.Meta.GetStatus(entity.Stat.Value).IsReadonly)
			{
				err = new Err { errorCode = ErrorCode.STATUS_READONLY };
				return false;
			}//if
			return true;
		}//function
		#endregion 

		#region joins
		/// <summary>
		/// грузить модель полностью, со всеми связями
		/// </summary>
		public bool LoadAllJoins = true;

		

		#endregion
	}//class
}//ns
