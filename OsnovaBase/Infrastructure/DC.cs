﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Infrastructure
{
	public class DC: AbstractContext
	{
		new public string typeName
		{
			get { return type == null ? string.Empty : type.Name; }
			set { type = Type.GetType("Osnova.Detached.{0}".Fmt(value)); }
		}

		new public DetachedBase entity { get { return _entity; } set { _entity = value; type = value.With(o => o.Type); }  }
		new protected DetachedBase _entity;

		public IEnumerable<DetachedBase> list = null;

		DetachedBase getNew()	{	return (DetachedBase)Activator.CreateInstance(type);	}//function

		public bool Create()
		{
			if (CheckUserIsSet() == false) { return false; }
			if (CheckUserCanCreate() == false) { return false; }

			try
			{
				entity = getNew();
				bool OK = entity.Create(this);
				return OK;
			}//try
			catch (Exception exception)
			{
				err = new Err { errorCode = ErrorCode.ENTITY_NOT_SAVED, exception = exception };
				return false;
			}//catch
		}//function

		public bool Read()
		{
			if (CheckUserIsSet() == false) { return false; }
			if (CheckUserCanRead() == false) { return false; }

			try
			{
				entity = getNew();
				bool OK = entity.Read(this);
				return OK;
			}//try
			catch (Exception exception)
			{
				err = new Err { errorCode = ErrorCode.ENTITY_NOT_FOUND, exception = exception };
				return false;
			}//catch
		}//function

		public bool Update()
		{
			if (CheckUserIsSet() == false) { return false; }
			if (CheckUserCanWrite() == false) { return false; }
			if (CheckEntityIsSet() == false) { return false; }

			try
			{
				bool OK = entity.Update(this);
				return OK;
			}//try
			catch (Exception exception)
			{
				err = new Err { errorCode = ErrorCode.ENTITY_NOT_SAVED, exception = exception };
				return false;
			}//catch
		}//function

		public bool Delete()
		{
			if (CheckUserIsSet() == false) { return false; }
			if (CheckUserCanWrite() == false) { return false; }
			if (CheckEntityIsSet() == false) { return false; }

			try
			{
				bool OK = entity.Delete(this);
				return OK;
			}//try
			catch (Exception exception)
			{
				err = new Err { errorCode = ErrorCode.ENTITY_NOT_DELETED, exception = exception };
				return false;
			}//catch
		}//function


		public bool Select()
		{
			if (CheckUserIsSet() == false) { return false; }
			if (CheckUserCanRead() == false) { return false; }

			try
			{
				entity = getNew();
				bool OK = entity.Select(this);
				return OK;
			}//try
			catch (Exception exception)
			{
				err = new Err { errorCode = ErrorCode.ENTITY_NOT_FOUND, exception = exception };
				return false;
			}//catch
		}//function
	}//class
}//ns
