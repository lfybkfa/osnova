﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Infrastructure
{
	/// <summary>
	/// часто используемые служебные строки
	/// </summary>
	public static class STR
	{
		public static readonly string[] EMPTY = new string[0];
		public static readonly string RETURN = Environment.NewLine;
		public static readonly string SPACE = " ";
		public static readonly string ZPT = ",";
	}//class

	/// <summary>
	/// часто используемые служебные символы
	/// </summary>
	public static class CHAR
	{
		public static readonly char[] DELIM = {' ', ',',';'};
		public static readonly char[] EMPTY = { };
		public static readonly char EQ = '=';
	}//class

	/// <summary>
	/// часто используемые слова
	/// </summary>
	public static class WORD
	{
		public static readonly string Add = "Add";
		public static readonly string All = "All";
		public static readonly string Code = "Code";
		public static readonly string Check = "Check";
		public static readonly string Delete = "Delete";
		public static readonly string End = "End";
		public static readonly string First = "First";
		public static readonly string From = "From";
		public static readonly string Last = "Last";
		public static readonly string Main = "Main";
		public static readonly string Name = "Name";
		public static readonly string Parameters = "Parameters";
		public static readonly string Read = "Read";
		public static readonly string ReadNot = "ReadNot";
		public static readonly string Readonly = "Readonly";
		public static readonly string Run = "Run";
		public static readonly string Remove = "Remove";
		public static readonly string Start = "Start";
		public static readonly string Write = "Write";
		public static readonly string WriteNot = "WriteNot";

		public static readonly string DESC = "DESC";
		public static readonly string ID = "ID";
		public static readonly string NULL = "NULL";
	}//class
}//ns
