﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Infrastructure
{
	public class Err
	{
		public DateTime dt = DateTime.Now;
		public string message = string.Empty;
		public ErrorCode errorCode;
		public Exception exception;
		public string exceptionInfo 
		{
			get {return exception != null ? exception.ToString() : "NO EXCEPTION";}
		}

		public string fullInfo 
		{ 
			get { return "{0} code={1} message={2} exception={3}".Fmt(dt, errorCode, message, exceptionInfo); } 
		}

		#region functions
		#endregion
	}//class

}//ns
