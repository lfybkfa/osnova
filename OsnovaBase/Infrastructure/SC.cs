﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Infrastructure
{
	/// <summary>
	/// Special context
	/// </summary>
	public class SC: AbstractContext
	{
		#region Properties
		static readonly string TypeNameFormat = "Osnova.Model.{0}Special";
		public delegate bool specialDelegate(SC specialContext);

		private Type typeWorker { get { return Type.GetType(typeWorkerName); } }
		private string typeWorkerName
		{
			get
			{
				if (type == null) { return string.Empty; }
				WorkerAttribute worker = type.GetWorker();
				if (worker == null) { return TypeNameFormat.Fmt(type.Name); }
				if (worker.Special != null) { return TypeNameFormat.Fmt(worker.Move); }
				return string.Empty;
			}
		}

		private specialDelegate run;
		public string Name;
		
		#endregion
		private bool Prepare()
		{
			if (run == null)
			{
				if (typeWorker == null)
				{
					err = new Err { errorCode = ErrorCode.TYPE_NOT_WORKER, message = "Type={1}".Fmt(typeWorkerName) };
					return false;
				}//if

				var methodInfo = typeWorker.GetMethod(Name);
				if (methodInfo == null)
				{
					err = new Err { errorCode = ErrorCode.METHOD_SPECIAL_NOT_FOUND, message = "Method={0}".Fmt(Name) };
					return false;
				}//if
				run = (specialDelegate)methodInfo.CreateDelegate(typeof(specialDelegate));
			}//if
			return true;
		}//function

		public bool Run()
		{
			if (CheckUserIsSet() == false) { return false; }
			if (CheckUserCanRead() == false) { return false; }

			if (AC.Meta.CheckUserCanRun(userContext, type.Name, Name) == false)
			{
				err = new Err { errorCode = ErrorCode.USER_NOT_RUN_ACCESS, message = "Type={0} Special={1}".Fmt(type.Name, Name) };
				return false;
			}//if

			try
			{
				//устанавливаем метод на основании типа и имени
				if (Prepare() == false)
					return false;

				//получаем результат
				if (run(this) == false)
				{
					err = new Err { errorCode = ErrorCode.SPECIAL_NOT_RUN };
					return false;
				}//if
			}//try
			catch (Exception exception)
			{
				err = new Err { errorCode = ErrorCode.SPECIAL_NOT_RUN, exception = exception };
				return false;
			}//catch
			return true;
		}//function

		private MethodInfo methodInfo
		{
			get
			{
				if (Prepare() == false)
					return null;
				else
					return run.GetMethodInfo();
			}//get
		}//prop

		[Obsolete]
		public IEnumerable<string> paramNames
		{
			get
			{
				var mi = methodInfo;
				if (mi == null)
					return null;
				else
					return mi.GetCustomAttributes<ParamAttribute>(false).Select(a => a.Name);
			}//get
		}//prop
	}//class
}//ns
