﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Model;

namespace Osnova.Infrastructure
{
	public interface ILogger
	{
		void Info(object Message);
		void Error(object Message);

		void Info(object Message, bool Yes);
		void Error(object Message, bool Yes);
	}//interface

	public interface ICache
	{
		void Add(EntityBase entity);
		EntityBase Get(Type type, int ID);
	}//interface

	/// <summary>
	/// Application Context
	/// Контекст приложения
	/// </summary>
	static class AC
	{
		public static readonly string ConnectionInfo = "Osnova.Infrastructure.OsnovaDB";
		public static readonly ILogger Logger = new SimpleLogger("simple_log_{0}.txt");
		public static Room Room {get; private set;}
		public static Meta Meta { get; private set; }

		public static void Initialize()
		{
			if (Room != null)
				return;

			Meta = new Meta();
			Meta.Refresh();

			Room = new Room();
		}//function

	}//class
}//ns
