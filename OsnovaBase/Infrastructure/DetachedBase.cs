﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Infrastructure
{
	/// <summary>
	/// класс для работы с произвольными источниками данных (БД, веб-сервис, файл итд)
	/// </summary>
	public abstract class DetachedBase
	{
		public long ID = 0L;
		public Type Type { get { return GetType(); } }

		/// <summary>
		/// функция создает объект в источнике, используя detachedContext.ParamGet(), и сохраняет его в detachedContext.entity
		/// </summary>
		/// <param name="detachedContext"></param>
		/// <returns></returns>
		public virtual bool Create(DC detachedContext) { return false; }

		/// <summary>
		/// функция загружает объект из источника, используя detachedContext.entityID, и сохраняет его в detachedContext.entity
		/// </summary>
		/// <param name="detachedContext"></param>
		/// <returns></returns>
		public virtual bool Read(DC detachedContext) { return false; }
		
		/// <summary>
		/// функция сохраняет объект в источник, используя detachedContext.entity
		/// </summary>
		/// <param name="detachedContext"></param>
		/// <returns></returns>
		public virtual bool Update(DC detachedContext) { return false; }

		/// <summary>
		/// функция удаляет объект из источника, используя detachedContext.entity
		/// </summary>
		/// <param name="detachedContext"></param>
		/// <returns></returns>
		public virtual bool Delete(DC detachedContext) { return false; }

		/// <summary>
		/// функция загружает список объектов из источника, используя detachedContext.ParamGet(), и сохраняет его в detachedContext.list
		/// </summary>
		/// <param name="detachedContext"></param>
		/// <returns></returns>
		public abstract bool Select(DC detachedContext);
	}//class
}//ns