﻿using Osnova.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Infrastructure
{
	//User Context
	public class UC
	{
		public readonly int UserId;
		public User User;
		public readonly DateTime dtCreated = DateTime.Now;
		public IReadOnlyCollection<int> roles { get; private set; }
		
		//буферы доступа к полям
		internal IDictionary<string, ISet<string>> FieldsRead;
		internal IDictionary<string, ISet<string>> FieldsWrite;

		public UC(int UserID)
		{
			this.UserId = UserID;
		}//constructor

		public bool Refresh()
		{
			User = OsnovaDB.GetOnID<User>(UserId);
			LC lc = new LC { ParentId = UserId };
			bool result = RoleLink.OnUser(lc);
			roles = lc.list.Select(r => r.ID).ToArray();
			return result;
		}//function

		public override string ToString()
		{
			if (User == null)
				return UserId.ToString();
			else
				return "{0} created {1}".Fmt(User, dtCreated);
		}//function
	}//class
}//ns
