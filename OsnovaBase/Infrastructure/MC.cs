﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Osnova.Model;

namespace Osnova.Infrastructure
{
	/// <summary>
	/// MC = MoveContext
	/// Класс для документооборота
	/// </summary>
	public class MC: AbstractContext
	{
		static readonly string TypeNameFormat = "Osnova.Model.{0}Move";
		private Type typeWorker { get { return Type.GetType(typeWorkerName); } }
		private string typeWorkerName 		{	get {
			if (type == null) { return string.Empty; }
			WorkerAttribute worker = type.GetWorker();
			if (worker == null) { return TypeNameFormat.Fmt(type.Name); }
			if (worker.Move != null) { return TypeNameFormat.Fmt(worker.Move); }
			return string.Empty;
		} }
		
		public delegate bool checkDelegate(MC moveContext);
		public delegate bool runDelegate(MC moveContext);
		private static readonly string fmtCheck = "check{0}";
		private static readonly string fmtRun = "run{0}";

		/// <summary>
		/// код статуса, из которого двигаем
		/// может быть пустым
		/// </summary>
		public string codeFrom;
		/// <summary>
		/// код статуса, в который двигаем
		/// обязателен
		/// </summary>
		public string codeTo;
		private Status StatusFrom;
		private Status StatusTo;

		private IList<checkDelegate> CheckFuncS;
		private IList<runDelegate> RunFuncS;

		private void FillStatus()
		{
			StatusFrom = (codeFrom == null || codeFrom == WORD.NULL) ? null : AC.Meta.GetStatus(dype.Code, codeFrom);
			StatusTo = (codeTo == null) ? null : AC.Meta.GetStatus(dype.Code, codeTo);
		}//function

		private bool Prepare()
		{
			FillStatus();
			
			if (StatusTo == null) 
			{
				err = new Err { errorCode = ErrorCode.STATUS_TO_NOT_FOUND, message = "statusCode={1}".Fmt(codeTo??string.Empty) };
				return false;
			}//if
			Type workerType = typeWorker;
			if (workerType == null)
			{
				err = new Err { errorCode = ErrorCode.TYPE_NOT_WORKER, message = "Type={1}".Fmt(typeWorkerName) };
				return false;
			}//if

			MethodInfo methodInfo;
			string methodName;
			if (StatusTo.HasCheck)
			{
				CheckFuncS = new List<checkDelegate>();
				foreach (var name in StatusTo.CheckS)
				{
					methodName = fmtCheck.Fmt(name);
					methodInfo = workerType.GetMethod(methodName);
					if (methodInfo == null)
					{
						err = new Err { errorCode = ErrorCode.METHOD_CHECK_NOT_FOUND, message = "Method={0}".Fmt(methodName) };
						return false;
					}//if
					CheckFuncS.Add( (checkDelegate)methodInfo.CreateDelegate(typeof(checkDelegate)) );
				}//for
			}//if
			else
			{
				CheckFuncS = null;
			}//else

			if (StatusTo.HasRun)
			{
				RunFuncS = new List<runDelegate>();
				foreach (var name in StatusTo.RunS)
				{
					methodName = fmtRun.Fmt(name);
					methodInfo = workerType.GetMethod(methodName);
					if (methodInfo == null)
					{
						err = new Err { errorCode = ErrorCode.METHOD_RUN_NOT_FOUND, message = "Method={0}".Fmt(methodName) };
						return false;
					}//if
					RunFuncS.Add((runDelegate)methodInfo.CreateDelegate(typeof(runDelegate)));
				}//for
			}//if
			else
			{
				RunFuncS = null;
			}//else
			return true;
		}//function

		public bool Move()
		{
			if (Prepare() == false) { return false; }
			if (CheckUserIsSet() == false) { return false; }
			if (CheckUserCanRead() == false) { return false; }
			if (CheckUserCanMove() == false) { return false; }
			if (CheckStatusFromTo() == false) { return false; }

			#region checks
			try
			{
				if (CheckFuncS != null)
				{
					foreach (var check in CheckFuncS)
					{
						if (check(this) == false)
						{
							err = new Err { errorCode = ErrorCode.MOVE_NOT_CHECKED, message = check.Method.Name };
							return false;
						}//if
					}//for
				}//if
			}//try
			catch (Exception exception)
			{
				err = new Err { errorCode = ErrorCode.MOVE_ERROR, exception = exception };
				return false;
			}//catch
			#endregion

			using(outerCtx = OsnovaDB.New)
			{
			using(tran = outerCtx.Database.BeginTransaction())
			{
				try 
				{
					if (RunFuncS != null)
					{
						foreach (var run in RunFuncS)
						{
							if (run(this) == false)
							{
								tran.Rollback();
								err = new Err { errorCode = ErrorCode.MOVE_NOT_RUN, message = run.Method.Name };
								return false;
							}//if
						}//for
					}//if
					#region UpdateStatus
					string sql;
					if (StatusFrom != null)
					{
						sql = "UPDATE ".Add(OsnovaDB.GetTableName(type)
							, @" SET Stat=@p0, UserChange=@p1, ChangeDate=getdate() WHERE ID=@p2 AND Stat=@p3");
						rows = outerCtx.RunSQLInTran(sql, StatusTo.ID, UserId, entityID, StatusFrom.ID);
					}//if
					else
					{
						sql = "UPDATE ".Add(OsnovaDB.GetTableName(type)
							, @" SET Stat=@p0, UserChange=@p1, ChangeDate=getdate() WHERE ID=@p2 AND Stat IS NULL");
						rows = outerCtx.RunSQLInTran(sql, StatusTo.ID, UserId, entityID);
						//rows = outerCtx.Database.ExecuteSqlCommand(TransactionalBehavior.EnsureTransaction, sql
							//, StatusTo.ID, UserId, entityID);
					}//else

					if (rows != 1)
					{
						tran.Rollback();
						err = new Err { errorCode = ErrorCode.STATUS_NOT_UPDATED };
						return false;
					}//if
					#endregion

					tran.Commit();
				}//try
				catch (Exception exception)
				{
					tran.Rollback();
					err = new Err { errorCode = ErrorCode.MOVE_ERROR, exception = exception };
					return false;
				}//catch
				finally
				{
					;
				}//fin
			}//using
			}//using
			return true;
		}//function

		/// <summary>
		/// служебная
		/// стирает статус у записи
		/// </summary>
		internal void ClearStatus() 
		{
			using (var ctx = OsnovaDB.New)
			{
				var sql = "UPDATE ".Add(OsnovaDB.GetTableName(type), " SET Stat = NULL WHERE ID = @p0");
				ctx.RunSQL(sql, entityID);
			}//using
		}//function

		/// <summary>
		/// проверка, что переход описан в настройках
		/// </summary>
		/// <returns></returns>
		private bool CheckStatusFromTo()
		{
			if (StatusTo.IsFirst == false && StatusFrom == null)
			{
				err = new Err { errorCode = ErrorCode.STATUS_FROM_BAD};
				return false;
			}//if

			if (StatusFrom != null && StatusTo.FromS.Contains(StatusFrom.Code) == false)
			{
				err = new Err { errorCode = ErrorCode.STATUS_FROM_BAD, message = StatusFrom.Code };
				return false;
			}//if

			return true;
		}//function

		/// <summary>
		/// проверка, что юзер имеет право к переводу в StatusTo
		/// </summary>
		/// <returns></returns>
		private bool CheckUserCanMove()
		{
			if (AC.Meta.CheckUserCanMove(userContext, type.Name, codeTo) == false)
			{
				err = new Err { errorCode = ErrorCode.USER_NOT_MOVE_ACCESS, message = "Type={0} Status={1}".Fmt(type.Name, codeTo) };
				return false;
			}//if
			return true;
		}//function

	}//class
}//ns
