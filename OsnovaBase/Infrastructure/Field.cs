﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Infrastructure
{
	public struct Field
	{
		public PropertyInfo property;
		public DisplayAttribute display;
		public FormatAttribute format;
		public string Name { get { return property.Name; } }
		public int Order { get { return display == null ? 0 : (display.GetOrder() ?? 0); } }
		public string GroupName { get { return display == null ? string.Empty : (display.GetGroupName() ?? string.Empty); } }
		public string Caption { get { return display == null ? Name : (display.GetName() ?? Name); } }
		public Type type { get { return property.PropertyType; } }
		public Type typeUnderNullable { get { return property.PropertyType.GetTypeUnderNullable(); } }
		//public bool IsNullable { get { return property.PropertyType.IsNullable(); } }
		public bool IsNullable; //заполнить для ускорения
		public TypeCode typeCode { get { return Type.GetTypeCode(typeUnderNullable); } }

		public override string ToString()	{		return "{0} {1} {2}".Fmt(Name, typeCode, IsNullable);	}//function

		public DataColumn CreateColumn()
		{
			DataColumn result;
			if (IsNullable)
			{ result = new DataColumn(Name, typeUnderNullable); }
			else
			{ result = new DataColumn(Name, type); }

			result.Caption = Caption;
			
			#region extension properties

			#endregion

			return result;
		}//function

		public static IEnumerable<Field> Get(Type type)
		{
			var propertyFlags = BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public;
			var allProperties = type.GetProperties(propertyFlags);

			return allProperties
					.Where(p => p.PropertyType.IsClass == false || p.PropertyType == typeof(string))
					.Select(p => new Field
					{
						property = p,
						IsNullable = p.PropertyType.IsNullable(),
						display = p.GetCustomAttribute<DisplayAttribute>(),
						format = p.GetCustomAttribute<FormatAttribute>(),
					});
		}//function
	}//class

}//ns
