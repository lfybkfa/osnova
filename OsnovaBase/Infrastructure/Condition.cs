﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Osnova.Infrastructure
{


	/// <summary>
	/// условие
	/// </summary>
	public class Condition
	{
		public string Name;
		public string Value;
		public string[] Values;
		public Comparision comp = Comparision.Equal;
		public string message;
		public ICollection<Condition> conditions; //для группы условий

		#region type work
		Type type;
		ParameterExpression Xentity;
		MemberExpression Xprop;
		
		PropertyInfo propInfo;
		Type propType;
		Type propTypeReal;
		object ValueConverted;
		object[] ValuesConverted;
		private void SetType(Type aType)
		{
			type = aType;
			if (type != null)
			{
				//Xentity для дочерних условий уже может быть задан, тогда заново его ставить нельзя
				Xentity = Xentity ?? Expression.Parameter(type, "entity");
				if (string.IsNullOrWhiteSpace(Name) == false)
				{
					propInfo = type.GetProperty(Name);
					if (propInfo != null)
					{
						Xprop = Expression.Property(Xentity, propInfo);
						propType = propInfo.PropertyType;
						propTypeReal = (propType.IsNullable()) ? Nullable.GetUnderlyingType(propType) : propType;
					}//if
				}//if
			}//if
		}//function

		private bool ConvertValue()
		{
			//для таких сравнений конверсия вообще не нужна
			if (goodWithoutConvert.Contains(comp))
			{
				return true;	
			}//if

			Type typeToConvert = (propTypeReal != null) ? propTypeReal : propType;
			try
			{
				if (comp == Comparision.IN)
				{
					if (Values != null && Values.Any() )
					{
						ValuesConverted = Values.Select(s => Convert.ChangeType(s, typeToConvert)).ToArray();
					}//if
					else if (Value.IsNothing() == false)
					{
						ValuesConverted = Value.Split(',').Select(s => Convert.ChangeType(s, typeToConvert)).ToArray();
					}//if
					else
					{
						message = "список пуст";
						return false;
					}//else
					
				}//if
				else
				{
					ValueConverted = Convert.ChangeType(Value, typeToConvert);
				}//else
				return true;
			}//try
			catch (Exception )
			{
				message = "Value {0} NOT converted tot Type {1}".Fmt(Value, typeToConvert);
				return false;
			}//catch
		}//function
		#endregion

		#region static dictionary
		static IDictionary<Comparision, ExpressionType> binaryAnalog = new Dictionary<Comparision, ExpressionType> 
		{
			{Comparision.Equal, ExpressionType.Equal},
			{Comparision.NotEqual, ExpressionType.NotEqual},
			{Comparision.GreaterThan, ExpressionType.GreaterThan},
			{Comparision.GreaterThanOrEqual, ExpressionType.GreaterThanOrEqual},
			{Comparision.LessThan, ExpressionType.LessThan},
			{Comparision.LessThanOrEqual, ExpressionType.LessThanOrEqual},
		};

		static Comparision[] goodWithoutConvert = { Comparision.CurrentDATE, Comparision.CurrentMONTH, Comparision.IsNULL, Comparision.CurrentYEAR, Comparision.OR };
		static Comparision[] goodForAll = { Comparision.Equal, Comparision.NotEqual, Comparision.IsNULL, Comparision.IN, Comparision.OR };
		static IDictionary<Type, IEnumerable<Comparision>> goodForType = new Dictionary<Type, IEnumerable<Comparision>> 
		{
			{
				typeof(int), new Comparision[] {Comparision.GreaterThan, Comparision.GreaterThanOrEqual, Comparision.LessThan, Comparision.LessThanOrEqual}
			},
			{
				typeof(string), new Comparision[] {Comparision.CONTAINS, Comparision.BEGIN, Comparision.END}
			},
			{
				typeof(DateTime), new Comparision[] {Comparision.GreaterThan, Comparision.GreaterThanOrEqual
					, Comparision.LessThan, Comparision.LessThanOrEqual
					, Comparision.CurrentDATE, Comparision.CurrentMONTH, Comparision.CurrentYEAR
					, Comparision.DATE, Comparision.MONTH, Comparision.YEAR}
			},
		};
		#endregion

		#region MEthodInfo
		private static readonly MethodInfo miStringContains = typeof(string).GetMethod("Contains");
		private static readonly MethodInfo miStringStartsWith = typeof(string).GetMethod("StartsWith", new[] {typeof(string)});
		private static readonly MethodInfo miStringEndsWith = typeof(string).GetMethod("EndsWith", new[] { typeof(string) });
		//	, BindingFlags.Instance |	BindingFlags.Public
		//	, null, new[] { typeof(string) }, null);
		#endregion

		public override string ToString()
		{
			return "{0} {1} {2}".Fmt(Name, comp, (Value != null) ? Value : 
				(Values != null ? Values.Length.ToString() : string.Empty));
		}//function

		public void addCondition(Condition c)
		{
			if (conditions == null) { conditions = new HashSet<Condition>(); }
			conditions.Add(c);
		}//function

		private Expression getBody<T>()
		{
			SetType(typeof(T));
			if (propInfo == null && comp != Comparision.OR) // при OR property вообще не требуется
			{
				message = "bad property {0}".Fmt(Name);
				return null;
			}//if

			//проверяем, что для данного типа приемлемо данное сравнение
			if (goodForAll.Contains(comp) == false)
			{
				if (propTypeReal == null || goodForType[propTypeReal] == null || !goodForType[propTypeReal].Contains(comp))
				{
					message = "Property type {0} is NOT compatible for comparision {1}".Fmt(propTypeReal, comp);
					return null;
				}//if
			}//if

			//пытаемся привести константы к нужному типу
			if (ConvertValue() == false)
				return null;

			if (binaryAnalog.Keys.Contains(comp))
			{
				var constant = Expression.Constant(ValueConverted, propType);
				return Expression.MakeBinary(binaryAnalog[comp], Xprop, constant);
			}//if
			else if (comp == Comparision.IsNULL)
			{
				return Expression.Equal(Xprop, Expression.Constant(null));
			}//if
			else if (comp == Comparision.IN)
			{
				var Xequals = ValuesConverted.Select(o => Expression.Equal(Xprop, Expression.Constant(o, propType)));
				return Xequals.Aggregate<Expression>((acc, eq) => Expression.Or(acc, eq));
			}//if
			else if (comp == Comparision.OR)
			{
				//устанавливаем общий тип дял дочерних условий
				conditions.ForEach(c => c.Xentity = Xentity);
				var Xexprs = conditions.Select(c => c.getBody<T>()).Where(expr => expr != null).ToArray();
				return Xexprs.Aggregate<Expression>((acc, eq) => Expression.Or(acc, eq));
			}//if
			#region contains begin end
			else if (comp == Comparision.CONTAINS)
			{ return Expression.Call(Xprop, miStringContains, Expression.Constant(ValueConverted)); }
			else if (comp == Comparision.BEGIN)
			{return Expression.Call(Xprop, miStringStartsWith, Expression.Constant(ValueConverted));}
			else if (comp == Comparision.END)
			{return Expression.Call(Xprop, miStringEndsWith, Expression.Constant(ValueConverted));}
			#endregion
			#region date
			else if (comp == Comparision.CurrentDATE)
			{
				DateTime dt = DateTime.Now.Date;
				return Xprop.Between(dt, dt.AddDays(1));
			}//if
			else if (comp == Comparision.CurrentMONTH)
			{
				DateTime now = DateTime.Now;
				DateTime dt = new DateTime(now.Year, now.Month, 1);
				return Xprop.Between(dt, dt.AddMonths(1));
			}//if
			else if (comp == Comparision.CurrentYEAR)
			{
				DateTime now = DateTime.Now;
				DateTime dt = new DateTime(now.Year, 1, 1);
				return Xprop.Between(dt, dt.AddYears(1));
			}//if
			else if (comp == Comparision.DATE)
			{
				DateTime dt = ((DateTime)ValueConverted).Date;
				return Xprop.Between(dt, dt.AddDays(1));
			}//if
			else if (comp == Comparision.MONTH)
			{
				DateTime dt = (DateTime)ValueConverted;
				DateTime dtFrom = new DateTime(dt.Year, dt.Month, 1);
				return Xprop.Between(dtFrom, dt.AddMonths(1));
			}//if
			else if (comp == Comparision.YEAR)
			{
				DateTime dt = (DateTime)ValueConverted;
				DateTime dtFrom = new DateTime(dt.Year, 1, 1);
				return Xprop.Between(dtFrom, dt.AddYears(1));
			}//if
			#endregion
			else
			{
				message = "не реализовано пока";
				return null;
			}//else
		}//function

		public Expression<Func<T,bool>> getExpr<T>()
		{
			Expression Xbody = getBody<T>();
			if (Xbody != null)
				return Expression.Lambda<Func<T, bool>>(Xbody, Xentity);
			else
				return null;
		}//function

		public Expression<Func<T,bool>> getExprOLD<T>()
		{
			SetType(typeof(T));
			if (propInfo == null && comp != Comparision.OR) // при OR property вообще не требуется
			{
				message = "bad property {0}".Fmt(Name);
				return null;
			}//if
			
			//проверяем, что для данного типа приемлемо данное сравнение
			if (goodForAll.Contains(comp) == false)
			{
				if (propTypeReal == null || goodForType[propTypeReal] == null || !goodForType[propTypeReal].Contains(comp))
				{
					message = "Property type {0} is NOT compatible for comparision {1}".Fmt(propTypeReal, comp);
					return null;
				}//if
			}//if

			//пытаемся привести константы к нужному типу
			if (ConvertValue() == false)
				return null;

			//стандартные сравнения - равно не равно больше меньше итд
			if (binaryAnalog.Keys.Contains(comp))
			{
				var constant = Expression.Constant(ValueConverted, propType);
				return Expression.Lambda<Func<T, bool>>(Expression.MakeBinary(binaryAnalog[comp], Xprop, constant), Xentity);
			}//if
			else if (comp == Comparision.IsNULL)
			{
				var constant = Expression.Constant(null);
				return Expression.Lambda<Func<T, bool>>(Expression.Equal(Xprop, constant), Xentity);
			}//if
			else if (comp == Comparision.IN)
			{
				var Xequals = ValuesConverted
					.Select(o => Expression.Equal(Xprop, Expression.Constant(o, propType)));
				var Xbody = Xequals.Aggregate<Expression>((acc, eq) => Expression.Or(acc, eq));
				return Expression.Lambda<Func<T, bool>>(Xbody, Xentity);
			}//if
			else if (comp == Comparision.OR)
			{
				var Xexprs = conditions.Select(c => c.getExpr<T>()).Where(expr => expr != null).ToArray(); //Expression<Func<T,bool>>[]
				var Xbody = Xexprs.Aggregate<Expression>((acc, eq) => Expression.Or(acc, eq));
				return Expression.Lambda<Func<T, bool>>(Xbody, Xentity);
			}//if
			#region contains begin end
			else if (comp == Comparision.CONTAINS)
			{
				var constant = Expression.Constant(ValueConverted);
				var Xcall = Expression.Call(Xprop, miStringContains, constant);
				return Expression.Lambda<Func<T, bool>>(Xcall, Xentity);
			}//if
			else if (comp == Comparision.BEGIN)
			{
				var constant = Expression.Constant(ValueConverted);
				var Xcall = Expression.Call(Xprop, miStringStartsWith, constant);
				return Expression.Lambda<Func<T, bool>>(Xcall, Xentity);
			}//if
			else if (comp == Comparision.END)
			{
				var constant = Expression.Constant(ValueConverted);
				var Xcall = Expression.Call(Xprop, miStringEndsWith, constant);
				return Expression.Lambda<Func<T, bool>>(Xcall, Xentity);
			}//if
			#endregion
			#region date
			else if (comp == Comparision.CurrentDATE)
			{
				DateTime now = DateTime.Now;
				var XdtFrom = Expression.Constant(now.Date);
				var XdtTo = Expression.Constant(now.Date.AddDays(1));
				var Xbody = Expression.And(
					Expression.GreaterThanOrEqual(Xprop, XdtFrom)
					, Expression.LessThan(Xprop, XdtTo));
				return Expression.Lambda<Func<T, bool>>(Xbody, Xentity);
			}//if
			else if (comp == Comparision.CurrentMONTH)
			{
				DateTime now = DateTime.Now;
				DateTime dt = new DateTime(now.Year, now.Month, 1);
				var XdtFrom = Expression.Constant(dt);
				var XdtTo = Expression.Constant(dt.AddMonths(1));
				var Xbody = Expression.And(
					Expression.GreaterThanOrEqual(Xprop, XdtFrom)
					, Expression.LessThan(Xprop, XdtTo));
				return Expression.Lambda<Func<T, bool>>(Xbody, Xentity);
			}//if
			else if (comp == Comparision.CurrentYEAR)
			{
				DateTime now = DateTime.Now;
				DateTime dt = new DateTime(now.Year, 1, 1);
				var XdtFrom = Expression.Constant(dt);
				var XdtTo = Expression.Constant(dt.AddYears(1));
				var Xbody = Expression.And(
					Expression.GreaterThanOrEqual(Xprop, XdtFrom)
					, Expression.LessThan(Xprop, XdtTo));
				return Expression.Lambda<Func<T, bool>>(Xbody, Xentity);
			}//if
			else if (comp == Comparision.DATE)
			{
				DateTime dt = (DateTime)ValueConverted;
				var XdtFrom = Expression.Constant(dt.Date);
				var XdtTo = Expression.Constant(dt.Date.AddDays(1));
				var Xbody = Expression.And(
					Expression.GreaterThanOrEqual(Xprop, XdtFrom)
					, Expression.LessThan(Xprop, XdtTo));
				return Expression.Lambda<Func<T, bool>>(Xbody, Xentity);
			}//if
			else if (comp == Comparision.MONTH)
			{
				DateTime dt = (DateTime)ValueConverted;
				DateTime dtFrom = new DateTime(dt.Year, dt.Month, 1);
				var XdtFrom = Expression.Constant(dtFrom);
				var XdtTo = Expression.Constant(dtFrom.AddMonths(1));
				var Xbody = Expression.And(
					Expression.GreaterThanOrEqual(Xprop, XdtFrom)
					, Expression.LessThan(Xprop, XdtTo));
				return Expression.Lambda<Func<T, bool>>(Xbody, Xentity);
			}//if
			else if (comp == Comparision.YEAR)
			{
				DateTime dt = (DateTime)ValueConverted;
				DateTime dtFrom = new DateTime(dt.Year, 1, 1);
				var XdtFrom = Expression.Constant(dtFrom);
				var XdtTo = Expression.Constant(dtFrom.AddYears(1));
				var Xbody = Expression.And(
					Expression.GreaterThanOrEqual(Xprop, XdtFrom)
					, Expression.LessThan(Xprop, XdtTo));
				return Expression.Lambda<Func<T, bool>>(Xbody, Xentity);
			}//if
			#endregion
			else
			{
				message = "не реализовано пока";
				return null;
			}//else
		}//function
	}//class

}//ns
