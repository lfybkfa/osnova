﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Reports;
using System.Data;
using Osnova.Model;

namespace Osnova.Infrastructure
{
	public interface IReportRunner { DataSet Run(RC reportContext);}

	/// <summary>
	/// ReportContext
	/// Usage: RC reportContext = new RC()
	/// </summary>
	public class RC: AbstractContext
	{
		#region properties
		/// <summary>
		/// результат
		/// </summary>
		public DataSet data = null;

		/// <summary>
		/// код отчета
		/// </summary>
		public string ReportCode = null;

		private Report _report = null;
		/// <summary>
		/// Отчет, полученный на основе ReportCode
		/// </summary>
		public Report report { get {
			if (_report == null)	{	_report = AC.Meta.GetReport(ReportCode);}//if
			return _report;
		} }
		#endregion

		public bool Run()
		{
			if (CheckUserIsSet() == false) { return false; }
			if (ReportCode.IsNothing())
			{
				err = new Err { errorCode = ErrorCode.REPORT_NOT_SET, message = "ReportCode is empty" };
				return false;
			}//if
			
			//берем из метаданных репорт, заодно проверяем что он такой есть
			if (report == null)
			{
				err = new Err { errorCode = ErrorCode.REPORT_NOT_EXIST, message = "Report {0} not exist".Fmt(ReportCode) };
				return false;
			}//if

			//проверяем доступ
			if (AC.Meta.CheckUserCanReport(userContext, report.ID) == false)
			{
				err = new Err { errorCode = ErrorCode.USER_NOT_REPORT_ACCESS, message = "ReportCode={0}".Fmt(ReportCode) };
				return false;
			}//if

			try
			{
				//устанавливаем класс отчета на основании кода
				Type typeReport = Type.GetType("Osnova.Reports." + ReportCode);
				if (typeReport == null)
				{
					err = new Err { errorCode = ErrorCode.REPORT_NOT_CLASS, message = "ReportCode={0}".Fmt(ReportCode) };
					return false;
				}//if

				//создаем экземпляр отчета
				IReportRunner runner = (IReportRunner)Activator.CreateInstance(typeReport);
				if (typeReport == null)
				{
					err = new Err { errorCode = ErrorCode.REPORT_INSTANCE_ERROR, message = "Type={0}".Fmt(typeReport.Name) };
					return false;
				}//if

				//получаем результат
				data = runner.Run(this);
				if (data == null)
				{
					err = new Err { errorCode = ErrorCode.REPORT_NOT_RUN };
					return false;
				}//if
			}//try
			catch (Exception exception)
			{
				err = new Err { errorCode = ErrorCode.REPORT_NOT_RUN, exception = exception };
				return false;
			}//catch
			return true;
		}

	}//class
}//ns
