﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using Osnova.Model;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.Infrastructure;

[assembly: InternalsVisibleTo("UnitTestProject")] //.Test_DbContext
namespace Osnova.Infrastructure
{
	public class OsnovaDB : DbContext
	{
		public OsnovaDB() : base() { }//function
		public OsnovaDB(string connectionInfo) : base(connectionInfo)	{ }//function
		public static OsnovaDB New { get { return new OsnovaDB(AC.ConnectionInfo); } }

		#region Sets
		public DbSet<Dype> DypeSet { get; set; }
		public DbSet<Groupdype> GroupdypeSet { get; set; }
		public DbSet<Role> RoleSet { get; set; }
		public DbSet<User> UserSet { get; set; }
		public DbSet<Special> SpecialSet { get; set; }
		public DbSet<Status> StatusSet { get; set; }
		public DbSet<Report> ReportSet { get; set; }

		//eco
		public DbSet<EcoProject> EcoProjectSet { get; set; }
		public DbSet<EcoIssue> EcoIssueSet { get; set; }
		public DbSet<EcoIssueLink> EcoIssueLinkSet { get; set; }
		public DbSet<EcoWorklog> EcoWorklogSet { get; set; }
		public DbSet<EcoCompany> EcoCompanySet { get; set; }
		public DbSet<EcoDepartment> EcoDepartmentSet { get; set; }
		public DbSet<EcoEmpl> EcoEmplSet { get; set; }
		public DbSet<EcoEmplAbsent> EcoEmplAbsentSet { get; set; }
		public DbSet<EcoEmplAbsentType> EcoEmplAbsentTypeSet { get; set; }

		//source
		public DbSet<SourceNews> SourceNewsSet { get; set; }
		#endregion

		#region LinkSets
		public DbSet<RoleDype> RoleDypeSet { get; set; }
		public DbSet<RoleSpecial> RoleSpecialSet { get; set; }
		public DbSet<RoleStatus> RoleStatusSet { get; set; }
		public DbSet<RoleStatusContent> RoleStatusContentSet { get; set; }
		public DbSet<RoleStatusRoute> RoleStatusRouteSet { get; set; }
		public DbSet<RoleUser> RoleUserSet { get; set; }
		public DbSet<RoleReport> RoleReportSet { get; set; }
		#endregion

		#region service
		public DbSet<Lock> LockSet { get; set; }
		public DbSet<Comment> CommentSet { get; set; }
		public DbSet<Ticket> TicketSet { get; set; }
		#endregion



		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			

			#region Types
			modelBuilder.Types().Configure(c => c.ToTable(GetTableName(c.ClrType)));
			#endregion

			#region Properties
			modelBuilder.Properties().Where(p => p.Name == "ID").Configure(p => p.IsKey());
			#endregion

			#region Foreign keys
			//modelBuilder.Entity<Dype>().HasF
			#endregion

			#region Settings
			modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
			modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
			#endregion

			#region Many to many

			//modelBuilder.Entity<User>().HasMany(u => u.RoleS).WithMany(r => r.UserS).Map(m => { new Many2Many { T1 = typeof(User), T2 = typeof(Role) }.Make(m); });
			#endregion
		}//function

		/// <summary>
		/// правило формрования имени таблицы для типа
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static string GetTableName(Type type)	{	return "Entity" + type.Name;	}//function
		public static string GetTableName<T>()		{	return "Entity" + typeof(T).Name;	}//function

		/// <summary>
		/// только укоротитель, смысла не содержит
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="args"></param>
		/// <returns>rows count</returns>
		public int RunSQL(string sql, params object[] args)
		{
			//AC.Logger.Info(sql);
			return Database.ExecuteSqlCommand(sql, args);
		}//function

		public int RunSQLInTran(string sql, params object[] args)
		{
			AC.Logger.Info(sql);
			return Database.ExecuteSqlCommand(TransactionalBehavior.EnsureTransaction, sql, args);
		}//function

		public IQueryable<T> SetNotrack<T>(params string[] includes) where T: EntityBase
		{
			return Set<T>().AsNoTracking();
		}//function

		public IQueryable SetNotrack(Type type)
		{
			return Set(type).AsNoTracking();
		}//function

		public DbQuery SetAsNoTracking(Type type)
		{
			return Set(type).AsNoTracking(); //(DbQuery<EntityBase>)
		}//function

		/// <summary>
		/// для вытаскивания объектов по коду (если такое поле у них есть)
		/// права игнорируются
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="Code">код</param>
		/// <returns>запись с нужным кодом или null</returns>
		public static T GetOnCode<T>(string Code) where T : EntityBase
		{
			if (typeof(T).GetProperty(WORD.Code) == null) {	return null;	}//if

			T result = null;
			using (var context = OsnovaDB.New)
			{
				string sql = "select * from {0} where Code = '{1}'".Fmt(GetTableName<T>(), Code);
				result = context.Set<T>().SqlQuery(sql).AsNoTracking().SingleOrDefault();
			}//using

			return result;
		}//function

		/// <summary>
		/// грузит первую же запись с нужным Name
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="Name"></param>
		/// <returns>null, если у объекта нет свойства Name или в БД нет записи с нужным Name </returns>
		public static T GetOnName<T>(string Name) where T : EntityBase
		{
			if (typeof(T).GetProperty(WORD.Name) == null) { return null; }//if

			T result = null;
			using (var context = OsnovaDB.New)
			{
				string sql = "select * from ".Add(GetTableName<T>(), " where Name = '", Name, "'");
				result = context.Set<T>().SqlQuery(sql).AsNoTracking().FirstOrDefault();
			}//using

			return result;
		}//function

		/// <summary>
		/// грузит запись по ИД
		/// права игнорируются
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="ID"></param>
		/// <returns></returns>
		public static T GetOnID<T>(int ID) where T : class
		{
			T result = null;
			try
			{
				using (var context = OsnovaDB.New)
				{
					result = context.Set<T>().Find(ID);
				}//using
			}//try
			catch { }//catch
			return result;
		}//function

		public T GetOnIDWithJoins<T>(int ID) where T : EntityBase
		{
			var joins = Dype.getJoins<T>().Select(pi => pi.Name);
			DbQuery<T> query = this.Set<T>().AsNoTracking();
			foreach (var item in joins)
			{
				query = query.Include(item);
			}//for
			var q = query.Where(z => z.ID == ID); 

			return q.FirstOrDefault();
		}//function

	}//class
	
}//ns
