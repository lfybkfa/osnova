﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osnova.Model;

namespace Osnova.Infrastructure
{
	class Room
	{
		private static object _lock = new object();
		private static Func<UC, int, bool> find = (uc, id) => uc.UserId == id;
		private ICollection<UC> users;
		public ILogger Logger = new SimpleLogger("logRoom{0}.txt");

		public UC Get(int UserID) { return users.FirstOrDefault(uc => uc.UserId == UserID); }//function
		public bool RefreshAll() { return users.Select(uc => uc.Refresh()).All(ok => ok); }//function

		public Room()
		{
			users = new HashSet<UC>();
		}//function

		public bool Invite(int UserID)
		{
			if (UserID <= 0) { return false; }
			if (users.Any(u => u.UserId == UserID)) { return false; }

			UC uc = new UC(UserID);
			bool result = uc.Refresh();
			lock (_lock) { users.Add(uc); }
			return result;
		}//function

		public bool FuckOff(int UserID)
		{
			UC userCtx = users.FirstOrDefault(uc => uc.UserId == UserID);
			if (userCtx != null)
			{
				lock (_lock)	{ users.Remove(userCtx); }
				return true;
			}//if
			else
				return false;
		}//function

		public void PrintContentToLog()
		{
			Role role;
			Logger.Info("Room on date {0}".Fmt(DateTime.Now.ToLongTimeString()));
			foreach (var uc in users)
			{
				Logger.Info(uc.User.ToString());
				foreach (var roleid in uc.roles)
				{
					role = AC.Meta.GetRole(roleid);
					if (role == null)
						Logger.Info("  Role error for {0}".Fmt(roleid));
					else
					{
						Logger.Info("  {0}".Fmt(role.ToString()));
						Logger.Info("    READ");
						foreach (var dype in AC.Meta.GetDypesOnRole(role.ID, AccessDype.READ))
						{
							Logger.Info("    {0}".Fmt(dype.ToString()));	
						}//for
						Logger.Info("    WRITE");
						foreach (var dype in AC.Meta.GetDypesOnRole(role.ID, AccessDype.WRITE))
						{
							Logger.Info("    {0}".Fmt(dype.ToString()));
						}//for
					}//else
				}//for
			}//for
		}//function
	}//class
}//ns
