﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Osnova.Infrastructure;
using Osnova.Model;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace Osnova
{
	public abstract class EntityBase
	{
		static readonly string TypeNameFormat = "Osnova.Model.{0}";

		[Display(Name = "ID", Order = -1)]
		public int ID { get; set; }

		[Timestamp]
		public byte[] RowVersion { get; set; }

		[Required]
		[Display(Name="Автор", Order=103, GroupName="Bla")]
		public int UserCreate { get; set; }

		[Required]
		[Display(Name = "Создан", Order = 104)]
		public DateTime CreateDate { get; set; }

		[Required]
		[Display(Name = "Редактор", Order = 105)]
		public int UserChange { get; set; }

		[Required]
		[Display(Name = "Исправлено", Order = 106)]
		public DateTime ChangeDate { get; set; }

		//документооборот
		[Display(Name = "Статус", Order = 107)]
		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public int? Stat { get; private set; }
		
		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		[Display(Name = "Ответственный", Order = 108)]
		public int? UserResponsible { get; private set; }

		//фиксация Idea=d150212
		[Display(Name = "Запрет", Order = 208)]
		public int? UserFix { get; private set; }

		/// <summary>
		/// Запускается перед самым сохранением
		/// </summary>
		/// <param name="UserId"></param>
		public virtual void SetUserAndDate(int UserId)
		{
			DateTime dt = DateTime.Now;
			ChangeDate = dt;
			UserChange = UserId;
			if (ID <= 0)
			{
				CreateDate = dt;
				UserCreate = UserId;
			}//if
		}//function

		public virtual IEnumerable<Err> Validate() { return null; }
		public virtual void AfterSave(EC ec) { ; }
		public virtual void BeforeSave(EC ec) { ; }
		public virtual void AfterLoad(EC ec) { ; }
		public virtual void AfterDelete(EC ec) { ; }
		public virtual void BeforeDelete(EC ec) { ; }
		public override string ToString() { return "Type={0}. ID={1}".Fmt(Type, ID); }//function
		public static Type GetType(string code) { return Type.GetType(TypeNameFormat.Fmt(code)); }//function

		[NotMapped]
		public Type Type { get { return this.GetType(); } }
		[NotMapped]
		public string Table { get { return OsnovaDB.GetTableName(this.Type); } }
		[NotMapped]
		public Dype MetaDype { get { return AC.Meta.GetDype(this.Type); } }

		/// <summary>
		/// фиксировать документ
		/// </summary>
		/// <param name="UserID">кто зафиксировал, NULL = снять фиксацию</param>
		public void Fix(int? UserID)
		{
			UserFix = UserID;
		}//function

		public EntityBase NavigationValue(string dypeCode)
		{
			EntityBase result = null;
			PropertyInfo pi = Type.GetProperty(dypeCode);
			if (pi == null) {return null;}

			result = (pi.GetValue(this) as EntityBase);
			return result;
		}//function
	}//class

	public class ComparerOfEntityBaseOnID: IEqualityComparer<EntityBase>
	{
		public bool Equals(EntityBase a, EntityBase b)
    {
        if (ReferenceEquals(a, b)) {return true;}
				if (ReferenceEquals(a, null) || ReferenceEquals(b, null)) { return false; }
        return a.ID == b.ID;
    }//function

    public int GetHashCode(EntityBase a)
    {
        if (ReferenceEquals(a, null)) return 0;
        return a.ID;
    }//function
	}//class
}//ns
