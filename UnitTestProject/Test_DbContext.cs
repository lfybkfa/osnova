﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Osnova;
using Osnova.Infrastructure;
using Osnova.Model;

namespace UnitTestProject
{
	[TestClass]
	public class Test_DbContext
	{
		int AdminId = 0;
		int ManagerId = 0;

		[TestInitialize]
		public void Test_Init()
		{
			AC.Initialize();
			ManagerId = User.GetOnLogin("Rulon").ID;
			AdminId = User.GetOnLogin("UAdmin").ID;
			AC.Room.Invite(ManagerId);
			AC.Room.Invite(AdminId);
		}//function

		[TestMethod]
		public void Test_Special()
		{
			string specialName = "Test";
			bool result;

			//create source
			EC ec = new EC { entity = new SourceNews { Name="rbc.ru", Url = "rbc.ru" }, UserId=ManagerId};
			result = ec.Save();
			Assert.IsTrue(result, ec.errInfo);

			//run no flag
			SC sc = new SC { Name = specialName, entity = ec.entity, UserId = ManagerId };
			result = sc.Run();
			Assert.IsFalse(result, sc.errInfo);

			//give flag
			RoleSpecial rs = new RoleSpecial { RoleID = AC.Meta.GetRole("Manager").ID, SpecialID = AC.Meta.GetSpecial("SourceNews", specialName).ID };
			EC ecRS = new EC { entity = rs, UserId = AdminId };
			result = ecRS.Save();
			Assert.IsTrue(result, ecRS.errInfo);

			//refresh from DB
			AC.Meta.Refresh();

			//run again
			result = sc.Run();
			Assert.IsTrue(result, sc.errInfo);

			//take flag
			result = ecRS.Delete();
			Assert.IsTrue(result, ecRS.errInfo);

			//delete source
			result = ec.Delete();
			Assert.IsTrue(result, ec.errInfo);
			return;
		}//function

		[TestMethod]
		public void Test_UserSaveAndDelete()
		{
			var ent = new User { Login = "Gagun Gugan", FirstName="qq", LastName="qq" };
			var ec = new EC { entity = ent, UserId = AdminId };
			
			bool result = ec.Save();
			Assert.IsTrue(result, ec.errInfo);

			result = ec.Delete();
			Assert.IsTrue(result, ec.errInfo);
		}//function

		[TestMethod]
		public void Test_UserLoadAndSave()
		{
			bool result;
			var ec = new EC { typeName="User", UserId = AdminId};
			result = ec.Load(11);
			AC.Logger.Info(ec.entityInfo);
			Assert.IsTrue(result, ec.errInfo);

			var user = ec.entity as User;
			user.FirstName = "Buratino";
			var ecSave = new EC { entity = user, UserId = AdminId };
			result = ecSave.Save();
			Assert.IsTrue(result, ecSave.errInfo);

			ec.type = typeof(User);
			result = ec.Load(122);
			Assert.IsFalse(result);

			AC.Logger.Info(ec.errInfo);
		}//function

		[TestMethod]
		public void Test_DeleteWithRef()
		{
			bool result;
			var ec = new EC { typeName = "Groupdype", UserId=AdminId};
			result = ec.Load(101);
			Assert.IsTrue(result, ec.errInfo);

			result = ec.Delete();
			Assert.IsFalse(result);
		}//function

		[TestMethod]
		public void Test_UserRole()
		{
			bool result = true;

			LC lu = new LC {  };
			result = lu.SelectAll<User>();
			Assert.IsTrue(result, lu.errInfo);

			LC lr = new LC { type = typeof(Role), name = "OnUser" };
			foreach (var user in lu.list)
			{
				AC.Logger.Info(user.ToString());
				lr.ParentId = user.ID;
				lr.Select();
				lr.list.ForEach(r => AC.Logger.Info("- " + r.ToString()));
			}//for
			Assert.IsTrue(result, lr.errInfo);
		}//function

		[TestMethod]
		public void Test_UserRoleModify()
		{
			bool result = true;
			Role role = OsnovaDB.GetOnCode<Role>("Manager");

			LC lr = new LC { type = typeof(Role), name="OnUser", ParentId = 11, UserId = AdminId };
			lr.Select();
			lr.list.ForEach(r => AC.Logger.Info("- " + r.ToString()));

			AC.Logger.Info(" REMOVE ");
			result = lr.Remove(role.ID);
			Assert.IsTrue(result, lr.errInfo);

			lr.Select();
			lr.list.ForEach(r => AC.Logger.Info("- " + r.ToString()));

			AC.Logger.Info(" ADD ");
			result = lr.Add(role.ID);
			Assert.IsTrue(result, lr.errInfo);

			lr.Select();
			lr.list.ForEach(r => AC.Logger.Info("- " + r.ToString()));
		}//function

		

		[TestMethod]
		public void Test_Enum()
		{
			AccessDype ad = AccessDype.NONE;
			ad = ad.Set(AccessDype.READ);
			Assert.IsTrue(ad.HasFlag(AccessDype.READ));

			ad = AccessDype.EDIT;
			ad = ad.Clear(AccessDype.READ);
			Assert.IsTrue(ad.HasFlag(AccessDype.READ) == false && ad.HasFlag(AccessDype.WRITE));
		}//function


		[TestMethod]
		public void Test_Room()
		{
			bool result = true;
			AC.Room.PrintContentToLog();
			Assert.IsTrue(result);
		}//function

		[TestMethod]
		public void Test_CreateAndChange()
		{
			bool result;

			//create source
			EC ec = new EC { entity = new SourceNews { Name = "rbc.ru", Url = "rbc.ru" }, UserId = ManagerId };
			result = ec.Save();
			Assert.IsTrue(result, ec.errInfo);

			int EntId = ec.entity.ID;
			ec = new EC { type = typeof(SourceNews), UserId = AdminId };
			result = ec.Load(EntId);
			Assert.IsTrue(result, ec.errInfo);

			Thread.Sleep(2000);

			(ec.entity as SourceNews).Name = "rbc " + DateTime.Now.Second.ToString();
			result = ec.Save();
			Assert.IsTrue(result);
		}//function

		[TestMethod]
		public void Test_Fix()
		{
			bool result = true;

			//создаем источник
			EC ec = new EC { entity = new SourceNews { Name = "rbc.ru", Url = "rbc.ru" }, UserId = ManagerId };
			result = ec.Save();
			Assert.IsTrue(result, ec.errInfo);

			//грузим его
			int EntId = ec.entity.ID;
			ec = new EC { type = typeof(SourceNews), UserId = AdminId };
			result = ec.Load(EntId);
			Assert.IsTrue(result, ec.errInfo);

			//фиксируем
			result = ec.Fix(true);
			Assert.IsTrue(result, ec.errInfo);
			EntityBase ent = ec.TakeEntityAndClear();

			//пытаемся сохранить и обламываемся
			ec = new EC { entity = ent, UserId = ManagerId };
			result = ec.Save();
			Assert.IsFalse(result, ec.errInfo);

			//пытаемся разфиксировать и обламываемся
			result = ec.Fix(false);
			Assert.IsFalse(result, ec.errInfo);

			//разфиксируем под админом
			ec = new EC { entity = ent, UserId = AdminId };
			result = ec.Fix(false);
			Assert.IsTrue(result);
		}//function

		[TestMethod]
		public void Test_Expr()
		{
			bool result = true;
			LC lc;
			//lc = new LC {type = typeof(Dype), name = "OnStatus", UserId = AdminId };
			//result = lc.Select();
		
			lc = new LC {  };
			//lc.AddCondition(new Condition { Name = "GroupdypeID", Value = "102a" });
			//lc.AddCondition(new Condition { Name = "StatusID", comp = Comparision.IsNULL });
			//lc.AddCondition(new Condition { Name = "Code", comp = Comparision.GreaterThanOrEqual, Value = "Role" });
			//lc.AddCondition(new Condition { Name = "Code", comp = Comparision.BEGIN, Value = "Role" });
			//lc.AddCondition(new Condition { Name = "Code", comp = Comparision.END, Value = "e" });
			//lc.AddCondition(new Condition { Name = "Code", comp = Comparision.IN, Values = new[] {"Dype", "Role"} });
			Condition cOR = new Condition { comp = Comparision.OR };
			cOR.addCondition(new Condition { Name = "Code", comp = Comparision.IN, Value = "Dype,Role" });
			cOR.addCondition(new Condition { Name = "Code", comp = Comparision.BEGIN, Value = "F" });
			lc.AddCondition(cOR);
			//lc.AddJoin<Groupdype>();
			lc.AddOrder("ChangeDate DESC");
			lc.AddOrder("ID");
			lc.AddOrder("Name");
			lc.pageSize = 10;
			result = lc.SelectAll<Dype>();
			Assert.IsTrue(lc.list.Any(), lc.errInfo);

			lc.pageNum++;
			result = lc.SelectAll<Dype>();
			Assert.IsFalse(lc.list.Any(), lc.errInfo);
		}//function

		[TestMethod]
		public void Test_Join()
		{
			bool result = true;
			LC lc;

			lc = new LC { };
			result = lc.SelectAll<RoleDype>();
			var rd = lc.list.Cast<RoleDype>().First();
			Assert.IsTrue(rd.Role != null && rd.Dype != null, lc.errInfo);

			lc = new LC { LoadAllJoins = false };
			result = lc.SelectAll<Dype>();
			var d = (Dype)lc.list.First();
			Assert.IsTrue(d.Groupdype == null);

			int ID = d.ID;
			
			//using (var context = OsnovaDB.New)		{		d = context.GetOnIDWithJoins<Dype>(ID);		}

			EC ec = new EC() { type = typeof(Dype), UserId = AdminId, LoadAllJoins = false };
			result = ec.Load(ID);
			d = (Dype)ec.entity;
			Assert.IsTrue(d.Groupdype == null);

			ec = new EC { type = typeof(Dype), UserId = AdminId };
			result = ec.Load(ID);
			d = (Dype)ec.entity;
			Assert.IsTrue(d.Groupdype != null);

		}//function

		[TestMethod]
		public void Test_SelectWithAccess()
		{
			AC.Logger.Info("=========== Test_SelectWithAccess");
			bool result;

			LC lc = new LC { typeName = "SourceNews", UserId = ManagerId };
			result = lc.Select();
			Assert.IsTrue(result, lc.errInfo);

			//AC.Room.PrintContentToLog();
			result = AC.Room.FuckOff(ManagerId);
			AC.Logger.Info("\t{0} removed {1}".Fmt(ManagerId, result));
			//AC.Room.PrintContentToLog();

			var ucm = AC.Room.Get(ManagerId);
			AC.Logger.Info(ucm, ucm != null);
			Assert.IsNull(ucm, "\tmanager must not be logined");

			lc = new LC { typeName = "SourceNews", UserId = ManagerId };
			result = lc.Select();
			Assert.IsFalse(result, lc.errInfo);

			AC.Room.Invite(ManagerId);
			lc = new LC { typeName = "SourceNews", UserId = ManagerId };
			result = lc.Select();
			Assert.IsTrue(result, lc.errInfo);
			AC.Logger.Info("=========== Test_SelectWithAccess  END");
		}//function

		[TestMethod]
		public void Test_Meta()
		{
			AC.Logger.Info("=============Test_Meta");
			var AdminRoleID = AC.Meta.GetRole("Admin");
			var dypeID = AC.Meta.GetDype("Role").ID;

			Assert.IsTrue(AC.Meta.GetDype("User").HasStatus && !AC.Meta.GetDype("Dype").HasStatus);

			var f2 = AC.Meta.GetDype("RoleDype").FieldsAll;
			f2.ForEach(item => AC.Logger.Info(item.ToString()));
			Assert.IsTrue(f2.First(f => f.Name == "UserCreate").display.Name == "Автор");
			AC.Logger.Info("=============Test_Meta END");
		}//function

		[TestMethod]
		public void Test_Fields()
		{
			var uc = AC.Room.Get(AdminId);
			var dypeCode = "Role";
			var fldRead = AC.Meta.GetUserFields(uc, dypeCode, AccessDype.READ);
			var fldWrite = AC.Meta.GetUserFields(uc, dypeCode, AccessDype.WRITE);
			Assert.IsTrue(fldRead.Contains(WORD.Code) && fldWrite.Contains(WORD.Name));

			dypeCode = "User";
			fldRead = AC.Meta.GetUserFields(uc, dypeCode, AccessDype.READ, statusCode: "Testing").ToArray();
			Assert.IsTrue(!fldRead.Contains("Login"));

			fldRead = AC.Meta.GetUserFields(uc, dypeCode, AccessDype.READ, statusCode: "Done").ToArray();
			Assert.IsTrue(fldRead.Contains("Login"));
		}//function

		[TestMethod]
		public void Test_DataSet()
		{
			AC.Logger.Info("=============Test_DataSet");
			var lc = new LC { UserId = AdminId };
			bool result = lc.SelectAll<Dype>();
			var ds = lc.CreateDataSet();
			Assert.IsTrue(ds.Tables[WORD.Main].Rows.Count > 0);

			var lc2 = new LC { UserId = AdminId };
			lc2.AddJoin<Groupdype>();
			result = lc2.SelectAll<Dype>();
			ds = lc2.CreateDataSet();
			ds.WriteXml("dype.xml");
			Assert.IsTrue(ds.Tables.Count > 1);

			var lc3 = new LC { UserId = AdminId };
			lc3.AddJoin<Role>();
			lc3.AddJoin<Status>();
			lc3.SelectAll<RoleStatusContent>();
			ds = lc3.CreateDataSet();
			ds.WriteXml("RoleStatusContent.xml");
			AC.Logger.Info("=============Test_DataSet END");
		}//function

		[TestMethod]
		public void Test_Comment()
		{
			AC.Logger.Info("============= Test_Comment");

			var ent = OsnovaDB.GetOnCode<Role>("Admin");
			Comment cm = Comment.CreateForEntity(ent);
			cm.UserId = DateTime.Now.Second%2 == 0 ?  ManagerId : AdminId;
			cm.Content = "Message from {0}".Fmt(DateTime.Now);
			cm.Save();

			var cmS = Comment.GetList(ent);
			Assert.IsTrue(cmS.Count() > 0);
			cmS.ForEach(c => AC.Logger.Info(c));
			AC.Logger.Info("============= Test_Comment END");
		}//function

		[TestMethod]
		public void Test_Lock()
		{
			AC.Logger.Info("============= Test_Lock");
			int entID = OsnovaDB.GetOnName<SourceNews>("rbc.ru").ID;

			bool result;
			var ecA = new EC { WithLock = true, UserId = AdminId, typeName = "SourceNews" };
			ecA.Load(entID);
			var ecM = new EC { WithLock = true, UserId = ManagerId, typeName = "SourceNews" };
			result = ecM.Load(entID);
			//ecM.entity = ecA.entity;
			//result = ecM.Save();
			Assert.IsFalse(result);
			AC.Logger.Info(ecM.errInfo);

			ecA.ClearLock = true;
			result = ecA.Save();
			Assert.IsTrue(result);
			
			//test concurency
			result = ecM.Save();
			Assert.IsFalse(result);
		
			AC.Logger.Info("============= Test_Lock END");
		}//function

		[TestMethod]
		public void Test_GetOnCode()
		{
			User u1 = OsnovaDB.GetOnCode<User>("bla-bla");
			Assert.IsNull(u1);

			Role r1 = OsnovaDB.GetOnCode<Role>("Manager");
			Assert.IsNotNull(r1);

			var ent = OsnovaDB.GetOnName<SourceNews>("rbc.ru");
			Assert.IsNotNull(ent);
		}//function

		[TestMethod]
		public void Test_Move()
		{
			AC.Logger.Info("============= Test_Move");
			bool result;
			Status st = AC.Meta.GetStatus<User>("Testing"); //from migration initial data
			Assert.IsTrue(st.IsFirst == false);

			AC.Logger.Info("очистка статуса");
			MC mc = new MC { type = typeof(User), entityID = ManagerId, codeTo = "Testing", UserId = AdminId };
			mc.ClearStatus();

			AC.Logger.Info("неверный переход");
			result = mc.Move();
			Assert.IsTrue(mc.errGet(ErrorCode.STATUS_FROM_BAD).Any(), mc.errInfo);
			AC.Logger.Info(mc.errInfo);
			AC.Logger.Info("=========");

			AC.Logger.Info("проверка все ОК");
			mc.codeTo = "Done";
			mc.ParamSet("Bukva", "BU");
			result = mc.Move();
			Assert.IsTrue(result, mc.errInfo);
			AC.Logger.Info(mc.errInfo);
			mc.ClearStatus();
			AC.Logger.Info("=========");

			AC.Logger.Info("плохая процедура и откат транзакции");
			mc.codeTo = "DoneBad";
			result = mc.Move();
			Assert.IsTrue(mc.errGet(ErrorCode.MOVE_NOT_RUN).Any(), mc.errInfo);
			AC.Logger.Info(mc.errInfo);
			mc.ClearStatus();
			AC.Logger.Info("=========");

			AC.Logger.Info("попытка перехода, не имея прав");
			mc.codeTo = "NoAccess";
			result = mc.Move();
			Assert.IsTrue(mc.errGet(ErrorCode.USER_NOT_MOVE_ACCESS).Any(), mc.errInfo);
			AC.Logger.Info(mc.errInfo);
			mc.ClearStatus();
			AC.Logger.Info("=========");

			AC.Logger.Info("переход в статус Readonly и попытка записи");
			mc.codeTo = "Done";
			result = mc.Move();
			Assert.IsTrue(result, mc.errInfo);
			EC ec = new EC();
			ec.CopyFrom(mc);
			ec.Load(ManagerId);
			(ec.entity as User).LastName += "BB";
			result = ec.Save();
			Assert.IsTrue(ec.errGet(ErrorCode.STATUS_READONLY).Any(), ec.errInfo);
			Assert.IsTrue(result == false);
			AC.Logger.Info(ec.errInfo);
			mc.ClearStatus();
			AC.Logger.Info("=========");


			AC.Logger.Info("============= Test_Move END");
		}//function

		[TestMethod]
		public void Test_Extension()
		{
			string s = null;
			int i;
			Assert.IsTrue(s.IsNothing());

			i = s.With(st => st.Length, 55);
			Assert.IsTrue(i == 55);
			s = "asd";
			i = s.With(st => st.Length, 66);
			Assert.IsTrue(i == 3);

			s = "Asd".AddLine("qwe", "zxc");
			Assert.IsTrue(s.IndexOf(Environment.NewLine) > 0);
		}//function


		[TestMethod]
		public void Test_SqlView()
		{
			string s = null;
			Assert.IsTrue(s.IsNothing());

			var list = OsnovaDB.New.Set<EcoIssue>().ToArray()  ;
			AC.Logger.Info(list.Count());
			Assert.IsTrue(list != null);
		}//function

		[TestMethod]
		public void Test_Report()
		{
			Action<string, string> View = (s1, s2) => s1 = s2;

			RC reportContext = new RC() { UserId = AdminId, ReportCode = "EcoTest" };
			//установка параметров из урла
			//reportContext.ParamSet(name, value);
			
			//запуск
			bool result = reportContext.Run();

			//if (reportContext.data != null)
			//{
			//	View(reportContext.ViewName, reportContext.data);
			//}//if
			//else if (reportContext.InvalidParameters)
			//{
			//	var model = reportContext.ParamNames.Select(s => new {key = s,value =  reportContext.ParamGet(s)).Concat(new {key = "Error", value = reportContext.err.message}
			//	View(reportContext.ViewParamName, model);
			//}//if

			reportContext.ParamSet("FromDate", DateTime.Now.AddMonths(-1));
			reportContext.ParamSet("ToDate", DateTime.Now);
			Assert.IsTrue(result == true);
		}//function

		[TestMethod]
		public void Test_Params()
		{
			Action<string> print = AC.Logger.Info ;
			print("=======");

			RC reportContext = new RC() { UserId = AdminId, ReportCode = "EcoTest", entityID = 3 };
			reportContext.ParamNames.ForEach(s => print(s + "=" + reportContext.ParamGet(s)));
			reportContext.ParamSet("FromDate", DateTime.Now.AddMonths(-1));
			reportContext.ParamSet("ToDate", DateTime.Now);
			print("=======");
			reportContext.ParamNames.ForEach(s => print(s + "=" + reportContext.ParamGet(s)));
			reportContext.ParamNames.ForEach(s => reportContext.ParamGet(s));
		}//function

		[TestMethod]
		public void Test_UserReadAccess()
		{
			LC lc = new LC { type = typeof(Dype), ParentId = ManagerId, name = "UserReadAccess", UserId = AdminId };
			bool OK = lc.Select();
			Assert.IsTrue(OK);
		}//function

	}//class
}//ns
